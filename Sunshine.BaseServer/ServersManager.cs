﻿using Sunshine.AuthServer;
using Sunshine.Mysql.Database;
using Sunshine.Protocol.Messages;
using Sunshine.BaseServer.Messages;
using Sunshine.WorldServer;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Sunshine.BaseClient;
using Sunshine.AuthServer.Client;
using System.Net.Sockets;
using Sunshine.WorldServer.Client;
using Sunshine.Protocol.Types;
using Sunshine.BaseServer.Loaders.World.Characters;
using Sunshine.BaseServer.Loaders.World.Monsters;
using Sunshine.BaseServer.Loaders.World.Maps;
using Sunshine.BaseServer.Loaders.World.Stats;
using Sunshine.BaseServer.Loaders.World.Spells;
using Sunshine.BaseServer.Loaders.Commands;
using Sunshine.BaseServer.Loaders.World.Effects;
using Sunshine.Protocol.Utils;
using Sunshine.BaseServer.Loaders.World.Items;
using Sunshine.MySql.Database.Managers;
using Sunshine.Logs;
using Sunshine.Protocol.Enums;
using Sunshine.AuthServer.Handlers.Connection;
using Sunshine.BaseServer.Loaders.World.Npcs;
using Sunshine.BaseServer.Loaders.World.BidsHouse;
using Sunshine.BaseServer.Loaders.World.Maps.Interactives;
using Sunshine.BaseServer.Loaders.World.Maps.Triggers;
using Sunshine.BaseServer.Loaders.World.Quests;

namespace Sunshine.Servers
{
    public class ServersManager : Singleton<ServersManager>
    {
        private AuthServer.AuthServer _authServer;
        private WorldServer.WorldServer _worldServer;
        private System.Timers.Timer _timer;

        public void Start()
        {
            _authServer = new AuthServer.AuthServer();
            _worldServer = new WorldServer.WorldServer();
            
            DatabaseManager.Initilize(); // Start MysqlConnection
            MessageInitializer.Initialize(); // Start Initializing of Messages
            ProtocolTypeManager.Initialize(); // Start Initializing of types
            EffectsLoader.Initialize(); // Start Loading of Effects
            SpellsLoader.Initialize(); // Start Loading of Spells
            ItemsLoader.Initialize(); // Start Loading of Items
            CommandsLoader.Initialize(); // Start Loading of Commands
            MapsLoader.Initialize(); // Start Loading of Maps
            TriggersLoader.Initialize(); // Start Loading of Triggers
            InteractivesLoader.Initialize(); // Starts Loading of Skills
            MonstersLoader.Initialize(); // Start Loading of MonsterSpawns
            DungeonsLoader.Initialize(); // Start Loading of Dungeons
            BidsHouseLoader.Initialize(); // Start Loading of BidsHouse;
            NpcsLoader.Initialize(); // Start Loading of NpcSpawns
            QuestsLoader.Initialize(); // Start Loading of Quests
            BreedsLoader.Initialize(); // Stats Loading of BreedStats
            CharactersLoader.Initialize(); // Start Loading of Characters
            _authServer.Initialize(); // Start AuthServer
            _worldServer.Initialize(); // Start WorldServer
            this.InitializeTimer(); // Start Timer to Save World
        }

        public IEnumerable<T> GetClients<T>() where T : class
        {
            if (typeof(T).Name == "AuthClient")
                return _authServer.authClients.Select(x => (T)x.Value);
            else
                return _worldServer.worldClients.Select(x => (T)x.Value);
        }

        public void AddClient(Socket sck, IBaseClient baseClient)
        {
            if (baseClient is AuthClient)
                _authServer.authClients.Add(sck, (AuthClient)baseClient);
            else
                _worldServer.worldClients.Add(sck, (WorldClient)baseClient);
        }

        public void RemoveClient(IBaseClient baseClient)
        {
            if (baseClient is AuthClient)
            {
                if (_authServer.authClients.Count > 0)
                {
                    if (baseClient != null && _authServer.authClients.ContainsValue(baseClient))
                    {
                        Socket sck = _authServer.authClients.FirstOrDefault(x => x.Value == baseClient).Key;
                        if (sck != null)
                        {
                            _authServer.authClients.Remove(sck);
                            sck.Disconnect(false);
                            sck.Dispose();
                        }
                    }
                }
            }
            else
            {
                if (_worldServer.worldClients.Count > 0)
                {
                    if (baseClient != null && _worldServer.worldClients.ContainsValue(baseClient))
                    {
                        Socket sck = _worldServer.worldClients.FirstOrDefault(x => x.Value == baseClient).Key;
                        if (sck != null)
                        {
                            _worldServer.worldClients.Remove(sck);
                            sck.Disconnect(false);
                            sck.Dispose();
                        }
                    }
                }
                if ((baseClient as WorldClient).Character != null)
                    (baseClient as WorldClient).Character.LogOut();
            }
        }

        public void UpdateServerStatus(ServerStatusEnum status)
        {
            foreach (AuthClient client in _authServer.authClients.Values)
                ConnectionHandler.SendServerStatusUpdateMessage(client, status);
        }

        public void SendAnnounce(TextInformationTypeEnum type, short messageId, params string[] parameters)
        {
            var characters = CharacterManager.Instance.Characters;

            foreach (var character in characters.Values.Where(x => x.IsInWorld))
                character.SendInformationMessage(type, messageId, parameters);
        }

        public void Save()
        {
            Logger.WriteInfo("Saving world...");
            SendAnnounce(TextInformationTypeEnum.TEXT_INFORMATION_ERROR, 164, new string[0]);
            UpdateServerStatus(ServerStatusEnum.SAVING);
            var characters = CharacterManager.Instance.Characters.Values;
            foreach (var character in characters)
                CharacterManager.Instance.Save(character);
            BidHouseManager.Instance.Save();
            SendAnnounce(TextInformationTypeEnum.TEXT_INFORMATION_ERROR, 165, new string[0]);
            UpdateServerStatus(ServerStatusEnum.ONLINE);
            Logger.WriteInfo("World saved !");
        }

        public void Save(object sender, System.Timers.ElapsedEventArgs e)
        {
            _timer.Stop();
            Logger.WriteInfo("Saving world...");
            SendAnnounce(TextInformationTypeEnum.TEXT_INFORMATION_ERROR, 164, new string[0]);
            UpdateServerStatus(ServerStatusEnum.SAVING);
            var characters = CharacterManager.Instance.Characters.Values;
            foreach(var character in characters)
                CharacterManager.Instance.Save(character);
            BidHouseManager.Instance.Save();
            SendAnnounce(TextInformationTypeEnum.TEXT_INFORMATION_ERROR, 165, new string[0]);
            UpdateServerStatus(ServerStatusEnum.ONLINE);
            Logger.WriteInfo("World saved !");
            _timer.Start();
        }

        private void InitializeTimer()
        {
            string[] config = System.IO.File.ReadAllLines(".\\Config.xml");
            double interval = int.Parse(config[12].Remove(0, 19));
            _timer = new System.Timers.Timer(interval);
            _timer.Elapsed += Save;
            _timer.Start();
        }
    }
}
