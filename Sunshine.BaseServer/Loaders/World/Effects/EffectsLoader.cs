﻿using Sunshine.Protocol.Enums;
using Sunshine.WorldServer.Game.Effects;
using Sunshine.WorldServer.Game.Effects.Spells;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;

namespace Sunshine.BaseServer.Loaders.World.Effects
{
    public static class EffectsLoader
    {
        public static void Initialize()
        {
            Logs.Logger.Write("[ World ] Loading Effects...");

            var effects = Assembly.GetAssembly(typeof(SpellEffectHandler)).GetTypes().Where(x => x.BaseType != null && x.BaseType.Name == "SpellEffectHandler");

            foreach(var effect in effects)
            {
                foreach (var attribute in effect.GetCustomAttributes())
                {
                    var effectId = (attribute as EffectHandler).Effect;
                    var currentEffect = effect.GetConstructor(Type.EmptyTypes);
                    var function = Expression.Lambda<Func<SpellEffectHandler>>(Expression.New(currentEffect));
                    EffectManager.Instance.SpellEffects.Add(effectId, function.Compile());
                }       
            }

            Logs.Logger.Write($"[ World ] {EffectManager.Instance.SpellEffects.Count} Effects Loaded");
        }
    }
}
