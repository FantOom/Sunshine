﻿using Sunshine.MySql.Database.Managers;
using Sunshine.WorldServer.Game.Actors.Monsters;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Sunshine.BaseServer.Loaders.World.Monsters
{
    public static class DungeonsLoader
    {
        public static void Initialize()
        {
            var maps = MapManager.Instance.Maps.Where(x => x.Value.IsDungeon && x.Value.Cells != null
                                                                             && x.Value.Cells.Count() > 0);
            foreach(var map in maps)
            {
                List<Monster> monsters = map.Value.Dungeon.Monsters.Select(x => new Monster(MonsterManager.Instance.GetMonster(x), MonsterManager.Instance.GetMonsterGrades(x))).ToList();               
                map.Value.RolePlayActors.Add(new MonsterGroup(monsters, map.Value, true));
            }                  
        }
    }
}
