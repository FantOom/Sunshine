﻿using Sunshine.MySql.Database.Managers;
using Sunshine.WorldServer.Game.Actors.Characters.Jobs;
using Sunshine.WorldServer.Game.Maps.Interactives;
using Sunshine.WorldServer.Game.Maps.Interactives.Skills;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;

namespace Sunshine.BaseServer.Loaders.World.Maps.Interactives
{
    public static class InteractivesLoader
    {
        public static void Initialize()
        {
            JobManager.Instance.JobSkills = InteractiveManager.Instance.GetAllInteractiveSkills();

            var skills = Assembly.GetAssembly(typeof(Skill)).GetTypes().Where(x => x.BaseType != null && x.BaseType.Name == "Skill");
            foreach(var skill in skills)
            {
                foreach (var skillAttribute in skill.GetCustomAttributes())
                {
                    var attribute = skillAttribute as SkillHandler;
                    var skillId = attribute.Id;
                    var currentSkill = skill.GetConstructor(Type.EmptyTypes);
                    var function = Expression.Lambda<Func<Skill>>(Expression.New(currentSkill));
                    SkillManager.Instance.Skills.Add(skillId, function.Compile());
                }
            }
        }
    }
}
