﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Sunshine.Protocol.Messages;
using Sunshine.WorldServer.Client;
using Sunshine.Protocol.Types;

namespace Sunshine.WorldServer.Handlers.PvP
{
    public class PvPHandler : WorldPacketHandler
    {
        [WorldHandler(1810)]
        public static void HandleSetEnablePVPRequestMessage(WorldClient client, SetEnablePVPRequestMessage message)
        {
            if (!client.Character.IsInFight)
                client.Character.TogglePvPMode(message.enable);
        }

        [WorldHandler(1811)]
        public static void HandleGetPVPActivationCostMessage(WorldClient client, GetPVPActivationCostMessage message)
        {
            client.Send(new PVPActivationCostMessage((short)(client.Character.Alignment.Honor / 35)));
        }

        [WorldHandler(5840)]
        public static void HandlePrismCurrentBonusRequestMessage(WorldClient client, PrismCurrentBonusRequestMessage message)
        {
            client.Send(new PrismAlignmentBonusResultMessage(new AlignmentBonusInformations(0, 0)));
        }

        [WorldHandler(5839)]
        public static void HandlePrismBalanceRequestMessage(WorldClient client, PrismBalanceRequestMessage message)
        {
            client.Send(new PrismBalanceResultMessage(0, 0));
        }

        public static void SendAlignmentRankUpdateMessage(WorldClient client)
        {
            client.Send(new AlignmentRankUpdateMessage(10, false));
        }
        public static void SendAlignmentSubAreasListMessage(WorldClient client)
        {
            client.Send(new AlignmentSubAreasListMessage(new short[0], new short[0]));
        }
    }
}
