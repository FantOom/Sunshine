﻿using Sunshine.Protocol.Messages;
using Sunshine.Protocol.Types;
using Sunshine.MySql.Database.Managers;
using Sunshine.WorldServer.Game;
using Sunshine.WorldServer.Client;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Sunshine.WorldServer.Game.Characters;
using Sunshine.Protocol.Enums;
using Sunshine.WorldServer.Game.Fights;
using Sunshine.WorldServer.Game.Actors.Fighters;
using Sunshine.WorldServer.Game.Actors;
using Sunshine.WorldServer.Game.Actors.Npcs;
using Sunshine.Protocol.Utils.Extensions;
using Sunshine.WorldServer.Game.Actors.Npcs.Actions;
using Sunshine.WorldServer.Game.Actors.Characters.Quests;

namespace Sunshine.WorldServer.Handlers.Context.Roleplay
{
    public class ContextRoleplayHandler : WorldPacketHandler
    {
        [WorldHandler(225)]
        public static void HandleMapInformationsRequestMessage(WorldClient client, MapInformationsRequestMessage message)
        {
            ContextRoleplayHandler.SendMapComplementaryInformationsDataMessage(client);
        }

        [WorldHandler(5610)]
        public static void HandleStatsUpgradeRequestMessage(WorldClient client, StatsUpgradeRequestMessage message)
        {
            StatsBoostTypeEnum statsBoost = (StatsBoostTypeEnum)message.statId;
            short boostPoint = message.boostPoint;
            int breedId = client.Character.Breed;
            short currentPoint = client.Character.Stats[CharacterManager.Instance.StatsRelations[statsBoost]].Base;

            if (statsBoost < StatsBoostTypeEnum.Strength || statsBoost > StatsBoostTypeEnum.Intelligence)
            {
                Logs.Logger.WriteError("Wrong statsid");
                return;
            }
            else
            {
                if (boostPoint > 0 && client.Character.StatsPoints >= boostPoint)
                {
                    client.Character.Stats[CharacterManager.Instance.StatsRelations[statsBoost]].Base += (short)BreedManager.Instance.SetStatsPoints(boostPoint, out int statsPoint, breedId, statsBoost, currentPoint);
                    client.Character.StatsPoints = (client.Character.StatsPoints - boostPoint) - statsPoint;
                    client.Send(new StatsUpgradeResultMessage(message.boostPoint));
                    client.Character.Stats.Update();
                    client.Character.RefreshStats();
                }
                else
                    client.Character.SendServerMessage("Not enough statsPoint !");
            }

        }

        [WorldHandler(221)]
        public static void HandleChangeMapMessage(WorldClient client, ChangeMapMessage message)
        {
            if (client.Character.Map.TopNeighbourId == message.mapId)
                client.Character.Cell.Id += 532;
            else if (client.Character.Map.BottomNeighbourId == message.mapId)
                client.Character.Cell.Id -= 532;
            else if (client.Character.Map.LeftNeighbourId == message.mapId)
                client.Character.Cell.Id += 13;
            else if (client.Character.Map.RightNeighbourId == message.mapId)
                client.Character.Cell.Id -= 13;

            client.Character.Teleport((int)message.mapId, client.Character.Cell.Id);
        }

        [WorldHandler(5731)]
        public static void HandleGameRolePlayPlayerFightRequestMessage(WorldClient client, GameRolePlayPlayerFightRequestMessage message)
        {
            Character target = CharacterManager.Instance.Characters[message.targetId];

            if (target != null)
            {
                if (message.friendly)
                {
                    FighterRefusedReasonEnum fighterRefusedReasonEnum = client.Character.CanRequestFight(target);
                    if (fighterRefusedReasonEnum != FighterRefusedReasonEnum.FIGHTER_ACCEPTED)
                        ContextHandler.SendChallengeFightJoinRefusedMessage(client, client.Character, fighterRefusedReasonEnum);
                    else
                        client.Character.SetFightRequest(FightTypeEnum.FIGHT_TYPE_CHALLENGE, target);
                }
                else
                {
                    FighterRefusedReasonEnum fighterRefusedReasonEnum = client.Character.CanAgress(target);
                    if (fighterRefusedReasonEnum != FighterRefusedReasonEnum.FIGHTER_ACCEPTED)
                        ContextHandler.SendChallengeFightJoinRefusedMessage(client, client.Character, fighterRefusedReasonEnum);
                    else
                    {
                        client.Character.SetFight(FightTypeEnum.FIGHT_TYPE_AGRESSION);
                        target.SetFight(FightTypeEnum.FIGHT_TYPE_AGRESSION, client.Character.Fight);
                        var fight = client.Character.Fight;
                        fight.AddFighter(client.Character.Fighter = new CharacterFighter(client.Character), true);
                        fight.AddFighter(target.Fighter = new CharacterFighter(target));
                    }
                }
            }
        }

        [WorldHandler(5732)]
        public static void HandleGameRolePlayPlayerFightFriendlyAnswerMessage(WorldClient client, GameRolePlayPlayerFightFriendlyAnswerMessage message)
        {
            Fight fight = client.Character.Fight;

            if (fight != null)
            {
                if (message.accept)
                {
                    ContextRoleplayHandler.SendGameRolePlayPlayerFightFriendlyAnsweredMessage(fight.Leader.Client, client.Character, fight.Leader, client.Character, true);
                    fight.AddFighter(fight.Leader.Fighter = new CharacterFighter(fight.Leader), true);
                    fight.AddFighter(client.Character.Fighter = new CharacterFighter(client.Character));
                }
                else
                {
                    ContextRoleplayHandler.SendGameRolePlayPlayerFightFriendlyAnsweredMessage(client, client.Character, fight.Leader, client.Character, false);
                    ContextRoleplayHandler.SendGameRolePlayPlayerFightFriendlyAnsweredMessage(fight.Leader.Client, client.Character, fight.Leader, client.Character, false);
                    fight.Leader.Fight = null;
                    client.Character.Fight = null;
                    FightManager.Instance.RemoveFight(fight);
                }
            }
        }

        [WorldHandler(5898)]
        public static void HandleNpcGenericActionRequestMessage(WorldClient client, NpcGenericActionRequestMessage message)
        {
            var npc = client.Character.Map.GetActor(message.npcId);
            if (npc != null && npc is Npc)
                (npc as Npc).InteractWith((NpcActionTypeEnum)message.npcActionId, client.Character);
            else
            {
                if (NpcManager.Instance.Npcs.ContainsKey(client.Character.Map.Id))
                    NpcManager.Instance.Npcs[client.Character.Map.Id].First().InteractWith((NpcActionTypeEnum)message.npcActionId, client.Character);
            }
        }

        [WorldHandler(5616)]
        public static void HandleNpcDialogReplyMessage(WorldClient client, NpcDialogReplyMessage message)
        {
            if (client.Character.IsInDialog && client.Character.Dialog is NpcTalkAction)
                (client.Character.Dialog as NpcTalkAction).ChangeMessage(message.replyId);
        }

        public static void SendNpcDialogCreationMessage(WorldClient client, Npc npc)
        {
            client.Send(new NpcDialogCreationMessage(client.Character.Map.Id, npc.Id));
        }

        public static void SendNpcDialogQuestionMessage(WorldClient client, Npc npc) // 238, 191
        {
            if (!npc.Record.HasQuest)
            {
                var visibleReplies = npc.GetDialogRepliesId[0].Select(x => short.Parse(x));
                client.Send(new NpcDialogQuestionMessage(npc.GetDialogMessagesId[0][0], new string[0], visibleReplies));
            }
            else
            {
               #region quests
                var questType = npc.GetNpcTypes.FirstOrDefault(x => x.Contains("-1")); // Quest 
                var index = npc.GetNpcTypes.IndexOf(questType);
                short questId = short.Parse(npc.GetParameters[index][0]);
                #endregion 

                if (!client.Character.Quests.HasQuest(questId)) // Hasn't quest
                {
                    var visibleReplies = npc.GetDialogRepliesId[0].Select(x => short.Parse(x));
                    client.Send(new NpcDialogQuestionMessage(npc.GetDialogMessagesId[0][0], new string[0], visibleReplies));
                }
                else
                {
                    if (!client.Character.Quests.GetQuest(questId).IsFinished) // Being quest
                        client.Send(new NpcDialogQuestionMessage(npc.GetDialogMessagesId[index][0], new string[0], new short[0]));
                    else
                    {
                        if (client.Character.Quests.GetQuest(questId).IsFinished &&
                            !client.Character.Quests.GetQuest(questId).isValided) // Being quest valided
                        {
                            var questTypeBeingFinsihed = npc.GetNpcTypes.FirstOrDefault(x => x.Contains("-2"));
                            var indexBeingFinished = npc.GetNpcTypes.IndexOf(questTypeBeingFinsihed);
                            client.Send(new NpcDialogQuestionMessage(npc.GetDialogMessagesId[indexBeingFinished][0], new string[0], new short[0]));
                            client.Character.Quests.GetQuest(questId).isValided = true;
                        }
                        else if (client.Character.Quests.GetQuest(questId).IsFinished) // Quest finished
                        {
                            var questTypeFinsihed = npc.GetNpcTypes.FirstOrDefault(x => x.Contains("-3"));
                            var indexFinished = npc.GetNpcTypes.IndexOf(questTypeFinsihed);
                            client.Send(new NpcDialogQuestionMessage(npc.GetDialogMessagesId[indexFinished][0], new string[0], new short[0]));
                        }
                    }
                }
            }
        }

        public static void SendNpcDialogQuestionMessage(WorldClient client, short messageId, 
            IEnumerable<string> dialogs, IEnumerable<short> replies) // 238, 191
        {
            client.Send(new NpcDialogQuestionMessage(messageId, dialogs, replies));
        }

        public static void SendEmoteListMessage(WorldClient client)
        {
            client.Send(new EmoteListMessage(new List<sbyte> { 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 19, 21, 22, 23, 24 }));
        }

        public static void SendCurrentMapMessage(WorldClient client, int mapid)
        {
            client.Send(new CurrentMapMessage(mapid));
        }

        public static void SendMapComplementaryInformationsDataMessage(WorldClient client)
        {
            var map = client.Character.Map;

            List<MapObstacle> obstacles = new List<MapObstacle>();
            foreach (var obstcl in map.Interactives.Select(x => x.GetObstacles))
                obstcl.ForEach(x => obstacles.Add(x));

            client.Send(new MapComplementaryInformationsDataMessage((short)map.SubAreaId, map.Id, 0, new List<HouseInformations>(), map.RolePlayActors.Select(x => x.GetGameRolePlayActorInformations),
                        map.Interactives.Where(x => x.GetInteractiveElement != null).Select(x => x.GetInteractiveElement), map.Interactives.Where(x => x.GetStatedElement != null).Select(x => x.GetStatedElement),
                        obstacles, map.Fights.Select(x => x.GetFightCommonInformations)));
            client.Character.EnterMap(map);
        }

        public static void SendGameRolePlayShowActorMessage(WorldClient client, RolePlayActor actor)
        {
            client.Send(new GameRolePlayShowActorMessage(actor.GetGameRolePlayActorInformations));
        }

        public static void SendGameRolePlayPlayerFightFriendlyRequestedMessage(WorldClient client, Character requester, Character source, Character target)
        {
            client.Send(new GameRolePlayPlayerFightFriendlyRequestedMessage(requester.Id, source.Id, target.Id));
        }

        public static void SendGameRolePlayPlayerFightFriendlyAnsweredMessage(WorldClient client, Character replier, Character source, Character target, bool accepted)
        {
            client.Send(new GameRolePlayPlayerFightFriendlyAnsweredMessage(replier.Id, source.Id, target.Id, accepted));
        }
    }
}
