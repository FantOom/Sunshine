﻿using Sunshine.Protocol.Enums;
using Sunshine.Protocol.Messages;
using Sunshine.WorldServer.Client;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Sunshine.WorldServer.Handlers.Dialogs
{
    public class DialogHandler : WorldPacketHandler
    {
        [WorldHandler(5501)]
        public static void HandleLeaveDialogRequestMessage(WorldClient client, LeaveDialogRequestMessage message)
        {
            if (client.Character.IsInTrade)
            {
                client.Character.Trade.Cancel();
                DialogHandler.SendLeaveDialogMessage(client);
            }
            else if(client.Character.IsInDialog)
            {
                client.Character.Dialog = null;
                DialogHandler.SendLeaveDialogMessage(client);
            }
        }

        public static void SendLeaveDialogMessage(WorldClient client)
        {
            client.Send(new LeaveDialogMessage());
        }
    }
}
