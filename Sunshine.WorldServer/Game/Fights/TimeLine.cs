﻿using Sunshine.WorldServer.Game.Actors.Fighters;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Sunshine.WorldServer.Game.Fights
{
    public class TimeLine
    {
        private Fight _fight;
        private int _roundNumber = 1;

        public TimeLine(Fight fight)
        {
            _fight = fight;
            Fighters = new List<FightActor>();
            Leavers = new List<FightActor>();
        }

        public List<FightActor> Fighters { get; set; }
        
        public List<FightActor> Leavers { get; set; }

        public int RoundNumber
        {
            get { return _roundNumber; }
            set { _roundNumber = value; }
        }
    }
}
