﻿using Sunshine.Protocol.Enums;
using Sunshine.Protocol.Types;
using Sunshine.WorldServer.Client;
using Sunshine.WorldServer.Game.Actors;
using Sunshine.WorldServer.Game.Actors.Fighters;
using Sunshine.WorldServer.Game.Fights.Teams;
using Sunshine.WorldServer.Game.Maps;
using Sunshine.WorldServer.Game.Spells;
using Sunshine.WorldServer.Handlers.Actions;
using Sunshine.WorldServer.Handlers.Context;
using System;
using System.Collections.Generic;
using Sunshine.Protocol.Utils.Extensions;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Timers;
using Sunshine.WorldServer.Game.Fights.Results;
using Sunshine.WorldServer.Game.Characters;
using Sunshine.WorldServer.Game.Fights.Types;

namespace Sunshine.WorldServer.Game.Fights
{
    public abstract class Fight
    {
        public abstract int Id { get; }

        public abstract FightTeam Team { get; }

        public abstract Map Map { get; }

        public abstract FightTypeEnum Type { get; }

        public abstract FightCommonInformations GetFightCommonInformations { get; }

        public abstract void AddFighter(FightActor fighter, bool isAttacker = false);

        public abstract int GetPlacementTimeLeft { get; }

        public abstract void CheckAllStatus();

        public abstract Timer Timer { get; set; }

        public Character Leader { get; set; }

        public short MaxMemberCount { get; set; }

        public FightResults Results { get; set; }

        public void CheckFightEnd()
        {
            if (Clients.Count <= 0 || Team.AreAllDead())
                EndFight();
        }

        public void ShowBlades()
        {
            ContextHandler.SendGameRolePlayShowChallengeMessage(Map.Clients, this);
        }

        public void HideBlades()
        {
            ContextHandler.GameRolePlayRemoveChallengeMessage(Map.Clients, this);
        }    

        public bool IsStarted { get { return State == FightStateEnum.Fighting; } }

        public FightStateEnum State { get; set; }

        public IEnumerable<int> GetAliveFightersIds { get { return TimeLine.Fighters.Where(x => x.IsAlive).Select(x => x.Id); } }

        public IEnumerable<int> GetDeadFightersIds { get { return TimeLine.Leavers.Select(x => x.Id); } }

        public List<WorldClient> Clients { get; set; }

        public TimeLine TimeLine { get; set; }

        public List<FightActor> Winners { get; set; }

        public List<FightActor> Losers { get; set; }

        public List<FightActor> GetAllFighters()
        {
            return TimeLine.Fighters.Concat(TimeLine.Leavers).ToList();
        }

        public IEnumerable<FightActor> GetAllFighters(Predicate<FightActor> predicate)
        {
            return from entry in GetAllFighters()
                   where predicate(entry)
                   select entry;
        }

        public IEnumerable<T> GetAllFighters<T>(System.Predicate<T> predicate) where T : FightActor
        {
            return from entry in TimeLine.Fighters.OfType<T>()
                where predicate(entry)
                select entry;
        }

        public FightActor GetOneFighter(int id)
        {
            return TimeLine.Fighters.FirstOrDefault(x => x.Id == id);
        }

        public FightActor GetOneFighter(short cell)
        {
            return TimeLine.Fighters.FirstOrDefault(x => x.IsAlive && x.Position.Cell == cell);
        }

        public FightActor FighterPlaying { get; set; }

        public FightActor GetFighterPlaying()
        {
            if (FighterPlaying == null)
            {
                ContextHandler.SendGameFightNewRoundMessage(Clients, TimeLine.RoundNumber++);
                return TimeLine.Fighters.FirstOrDefault();
            }
            else
            {
                int indexFighter = TimeLine.Fighters.IndexOf(FighterPlaying);
                if (TimeLine.Fighters.Count > indexFighter + 1)
                    return TimeLine.Fighters[indexFighter + 1];
                else
                {
                    ContextHandler.SendGameFightNewRoundMessage(Clients, TimeLine.RoundNumber++);
                    return TimeLine.Fighters.FirstOrDefault();
                }
            }
        }

        public void StartFight()
        {
            State = FightStateEnum.Fighting;
            HideBlades();
            SortFighters();
            ContextHandler.SendGameEntitiesDispositionMessage(Clients, GetAllFighters());
            ContextHandler.SendGameFightStartMessage(Clients);
            ContextHandler.SendGameFightTurnListMessage(Clients, this);
            ContextHandler.SendGameFightSynchronizeMessage(Clients, GetAllFighters());
            FighterPlaying = GetFighterPlaying();
            FighterPlaying.StartTurn();
        }

        public void EndFight()
        {
            Timer.Dispose();
            State = FightStateEnum.Ended;
            DeterminsWinners();
            Results.Apply();
            ContextHandler.SendGameFightEndMessage(Clients, this, Results.FightResultsListEntry);
            foreach (var fighter in GetAllFighters())
            {
                fighter.ResetFightProperties();
                if (fighter is CharacterFighter)
                {
                    var character = (fighter as CharacterFighter).Character;
                    Clients.Remove(character.Client);
                    ContextHandler.SendGameContextDestroyMessage(character.Client);
                    ContextHandler.SendGameContextCreateMessage(character.Client, GameContextEnum.ROLE_PLAY);
                    character.RefreshStats();
                    if (this is FightPvM && Map.IsDungeon)
                    {
                        character.Teleport(Map.Dungeon.NextMap, Map.Dungeon.NextCell);
                        character.Direction = Map.Dungeon.NextDirection;
                        ContextHandler.SendGameMapChangeOrientationMessage(character.Client);
                    }
                    else
                        character.Teleport(character.Map.Id, character.Cell.Id);
                    character.Fighter = null;
                    character.Fight = null;
                }
            }
            ContextHandler.SendGameMapFightCountMessage(Clients, (short)Map.Fights.Count);
            FightManager.Instance.RemoveFight(this);
        }

        public void EnterFighter(WorldClient client, FightActor fighter)
        {
            client.Character.StopRegen();
            ContextHandler.SendGameContextDestroyMessage(client);
            ContextHandler.SendGameContextCreateMessage(client, GameContextEnum.FIGHT);
            ContextHandler.SendGameFightStartingMessage(client, Type);
            ContextHandler.SendGameFightJoinMessage(client, this);
            ContextHandler.SendGameFightPlacementPossiblePositionsMessage(client, this);
            ContextHandler.SendGameFightShowFighterPreparationMessage(Clients, GetAllFighters());
            ContextHandler.SendGameEntitiesDispositionMessage(Clients, GetAllFighters());
            ContextHandler.SendGameFightUpdateTeamMessage(Clients, Team);
            ContextHandler.SendGameFightTurnListMessage(Clients, this);
        }

        public void LeaveFighter(WorldClient client, FightActor fighter)
        {
            if (Clients.Count > 1 && Type != FightTypeEnum.FIGHT_TYPE_AGRESSION)
            {
                fighter.OnDead(fighter, fighter);
                fighter.ResetFightProperties();
                var character = (fighter as CharacterFighter).Character;
                Clients.Remove(client);
                ContextHandler.SendGameContextDestroyMessage(client);
                ContextHandler.SendGameContextCreateMessage(client, GameContextEnum.ROLE_PLAY);
                character.RefreshStats();
                character.Teleport(character.Map.Id, character.Cell.Id);
                character.Fight = null;
            }
            else
            {
                EndFight();
                HideBlades();
            }
        }

        public void StartSequence(SequenceTypeEnum sequenceType)
        {
            ActionsHandler.SendSequenceStartMessage(Clients, FighterPlaying, sequenceType);
        }

        public void EndSequence(SequenceTypeEnum sequenceType, ActionsEnum actionType)
        {
            ActionsHandler.SendSequenceEndMessage(Clients, FighterPlaying, sequenceType, actionType);
        }

        public void OnFightPointsVariation(ActionsEnum action, FightActor source, FightActor target, int delta)
        {
            ActionsHandler.SendGameActionFightPointsVariationMessage(Clients, action, source, target, (short)delta);
        }

        public void OnLifePointsChanged(int delta, FightActor source, FightActor target)
        {
            ActionsHandler.SendGameActionFightLifePointsLostMessage(Clients, source, target, (short)delta);
        }

        public void OnShieldPointsChanged(int delta, int shliedLoss, FightActor source, FightActor target)
        {
            ActionsHandler.SendGameActionFightShieldPointsVariationMessage(Clients, source, target, (short)delta);
        }

        public void OnSpellCasting(FightActor caster, Spell spell, short cell, FightSpellCastCriticalEnum critical, bool silentCast)
        {
            ContextHandler.SendGameActionFightSpellCastMessage(Clients, ActionsEnum.ACTION_FIGHT_CAST_SPELL, caster, spell, cell, critical, false);
        }

        private void SortFighters()
        {
            #region GameFightMinimalStatsPreparation
            int fighterCount = TimeLine.Fighters.Count;
            List<FightActor> redFighters = new List<FightActor>();
            List<FightActor> blueFighters = new List<FightActor>();
            TimeLine.Fighters = new List<FightActor>();

            if (Team.Attackers.TrueForAll(x => x.GetGameFightMinimalStatsPreparation.initiative == 0)
              && Team.Defenders.TrueForAll(x => x.GetGameFightMinimalStatsPreparation.initiative == 0))
            {
                redFighters = Team.Attackers.OrderByDescending(x => x.Level).ToList();
                blueFighters = Team.Defenders.OrderByDescending(x => x.Level).ToList();

                if (redFighters[0].Level > blueFighters[0].Level)
                {
                    for (int i = 0; i < fighterCount; i++)
                    {
                        if (i < redFighters.Count)
                            TimeLine.Fighters.Add(redFighters[i]);
                        if (i < blueFighters.Count)
                            TimeLine.Fighters.Add(blueFighters[i]);
                    }
                }
                else
                {
                    for (int i = 0; i < fighterCount; i++)
                    {
                        if (i < blueFighters.Count)
                            TimeLine.Fighters.Add(blueFighters[i]);
                        if (i < redFighters.Count)
                            TimeLine.Fighters.Add(redFighters[i]);
                    }
                }
            }
            else
            {
                redFighters = Team.Attackers.OrderByDescending(x => x.GetGameFightMinimalStatsPreparation.initiative).ToList();
                blueFighters = Team.Defenders.OrderByDescending(x => x.GetGameFightMinimalStatsPreparation.initiative).ToList();

                if (redFighters[0].GetGameFightMinimalStatsPreparation.initiative
                  > blueFighters[0].GetGameFightMinimalStatsPreparation.initiative)
                {
                    for (int i = 0; i < fighterCount; i++)
                    {
                        if (i < redFighters.Count)
                            TimeLine.Fighters.Add(redFighters[i]);
                        if (i < blueFighters.Count)
                            TimeLine.Fighters.Add(blueFighters[i]);
                    }
                }
                else
                {
                    for (int i = 0; i < fighterCount; i++)
                    {
                        if (i < blueFighters.Count)
                            TimeLine.Fighters.Add(blueFighters[i]);
                        if (i < redFighters.Count)
                            TimeLine.Fighters.Add(redFighters[i]);
                    }
                }
            }
            #endregion
        }

        private void DeterminsWinners()
        {
            if (Team.Defenders.TrueForAll(x => x.IsDead))
            {
                Winners = Team.Attackers;
                Losers = Team.Defenders;
            }
            else
            {
                Winners = Team.Defenders;
                Losers = Team.Attackers;
            }
        }

        public void StartAction(double interval, object parameter)
        {
            if (Timer != null)
                Timer.Dispose();
            Timer = new Timer(interval);
            Timer.Elapsed += (sender, e) => ActionElapsed(parameter, e);
            Timer.Start();
        }

        private void ActionElapsed(object sender, ElapsedEventArgs e)
        {
            switch ((string)sender)
            {
                case "EndTurn":
                    FighterPlaying.EndTurn();
                    break;

                case "StartFight":
                    if (TimeLine.Fighters.Count <= 0)
                        EndFight();
                    else
                        StartFight();
                    break;
            }
        }
    }
}
