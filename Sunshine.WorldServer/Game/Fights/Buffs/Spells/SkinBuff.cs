﻿using Sunshine.Protocol.Types;
using Sunshine.WorldServer.Game.Actors;
using Sunshine.WorldServer.Game.Actors.Fighters;
using Sunshine.WorldServer.Game.Spells;
using Sunshine.WorldServer.Handlers.Actions;
using Sunshine.WorldServer.Handlers.Context;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Sunshine.WorldServer.Game.Fights.Buffs.Spells
{
    public class SkinBuff : Buff
    {
        public ActorLook Look { get; set; }

        public ActorLook OriginalLook { get; set; }

        public SkinBuff(FightActor caster, FightActor target, Spell spell,
            short duration, bool dispellable, ActorLook look)
        {
            Caster = caster;
            Target = target;
            Look = look;
            OriginalLook = Target.Look;
            Spell = spell;
            Duration = duration;
            Dispellable = dispellable;
        }

        public override void Apply()
        {
            //ActionsHandler.SendGameActionFightChangeLookMessage(Target.Fight.Clients, Caster, Target, );
        }

        public override void Dispell()
        {
            //ActionsHandler.SendGameActionFightChangeLookMessage(Target.Fight.Clients, Caster, Target, OriginalLook);
        }
    }
}
