﻿using Sunshine.MySql.Database.Managers;
using Sunshine.Protocol.Enums;
using Sunshine.Protocol.Types;
using Sunshine.WorldServer.Game.Actors.Characters.Quests;
using Sunshine.WorldServer.Game.Actors.Fighters;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Sunshine.WorldServer.Game.Fights.Results
{
    public class FightResults
    {
        private Fight _fight;
        private List<FightResultAdditionalData> _resultAdditional;
        private List<short> _items;
        private int _kamas;
        private IEnumerable<FightActor> _monsters;
        private IEnumerable<FightActor> _characters;

        public FightResults(Fight fight)
        {
            _fight = fight;
            _characters = new List<FightActor>();
            _monsters = new List<FightActor>();
            FightResultsListEntry = new List<FightResultListEntry>();
        }

        public List<FightResultListEntry> FightResultsListEntry { get; set; }

        public void Apply()
        {
            _characters = _fight.GetAllFighters(x => x is CharacterFighter);
            _monsters = _fight.GetAllFighters(x => x is MonsterFighter);

            foreach (var fighter in _fight.GetAllFighters())
            {
                if (_fight.Winners.Contains(fighter))
                    AddWinnerFighter(fighter);
                else
                    AddLoserFighter(fighter);
            }
        }

        private void AddWinnerFighter(FightActor fighter)
        {
            _resultAdditional = new List<FightResultAdditionalData>();
            _items = new List<short>();

            if (fighter is CharacterFighter)
            {
                AddEarnedExperience(fighter as CharacterFighter);
                AddDroppedItems(fighter as CharacterFighter);
                AddEarnedHonor(fighter as CharacterFighter);
                QuestManager.Instance.VerifyQuest((fighter as CharacterFighter).Character, QuestTypeEnum.QUEST_TYPE_WIN_VS_MONSTER_ONE_FIGHT, _monsters);

                FightResultsListEntry.Add(new FightResultPlayerListEntry((short)FightOutcomeEnum.RESULT_VICTORY, new FightLoot(_items, _kamas),
                    fighter.Id, fighter.IsAlive, (byte)fighter.Level, _resultAdditional));
            }
            else
                FightResultsListEntry.Add(new FightResultFighterListEntry((short)FightOutcomeEnum.RESULT_VICTORY, new FightLoot(new List<short>(), 0),
                    fighter.Id, fighter.IsAlive));
        }

        private void AddLoserFighter(FightActor fighter)
        {
            if (fighter is CharacterFighter)
            {
                AddLostedHonor(fighter as CharacterFighter);

                FightResultsListEntry.Add(new FightResultPlayerListEntry((short)FightOutcomeEnum.RESULT_LOST, new FightLoot(new List<short>(), 0),
                    fighter.Id, fighter.IsAlive, (byte)fighter.Level, new List<FightResultAdditionalData>()));
            }
            else
                FightResultsListEntry.Add(new FightResultFighterListEntry((short)FightOutcomeEnum.RESULT_LOST, new FightLoot(new List<short>(), 0),
                    fighter.Id, fighter.IsAlive));

        }

        private void AddDroppedItems(CharacterFighter fighter)
        {
            if (_fight.Type == FightTypeEnum.FIGHT_TYPE_PvM)
            {
                foreach (MonsterFighter monster in _monsters)
                {
                    foreach (var drop in monster.Drops)
                    {
                        int quantity = FightFormulas.CalculateWinItems(drop, monster.Grade.GradeId, fighter.Stats.Prospecting.Total);
                        if (quantity > 0)
                        {
                            var item = ItemManager.Instance.CreatePlayerItem(drop.ItemId, quantity);
                            fighter.Character.Inventory.AddItem(item, quantity);

                            if (_items.Contains((short)drop.ItemId))
                            {
                                var indexStack = _items.IndexOf((short)drop.ItemId);
                                _items[indexStack + 1] += (short)quantity;
                            }
                            else
                            {
                                _items.Add((short)drop.ItemId);
                                _items.Add((short)quantity);
                            }
                        }
                    }
                }
            }
        }

        private void AddEarnedExperience(CharacterFighter fighter)
        {
            if (_fight.Type == FightTypeEnum.FIGHT_TYPE_PvM)
            {
                long totalExp = _monsters.Sum(x => (x as MonsterFighter).Grade.GradeExp);
                int totalLevelCharacters = _characters.Sum(x => (x as CharacterFighter).Level);
                int totalLevelMonsters = _monsters.Sum(x => (x as MonsterFighter).Level);

                var expAdded = FightFormulas.CalculateWinExp(fighter.Stats[StatsEnum.Wisdom].Total, 0, 0, totalLevelMonsters,
                    totalLevelCharacters, totalExp, (byte)_characters.Count());

                fighter.Character.AddExperience(expAdded);

                _resultAdditional.Add(new FightResultExperienceData(true, true, true, true, false, false, false,
                fighter.Character.Experience, fighter.Character.ExperienceLevelFloor, fighter.Character.ExperienceNextLevelFloor, (int)expAdded, 0, 0));
            }
        }

        private void AddEarnedHonor(CharacterFighter fighter)
        {
            if (_fight.Type == FightTypeEnum.FIGHT_TYPE_AGRESSION)
            {
                fighter.Character.AddHonor(100);

                _resultAdditional.Add(new FightResultPvpData((byte)fighter.Character.Alignment.Grade, fighter.Character.Alignment.HonorGradeFloor,
                    fighter.Character.Alignment.HonorNextGradeFloor, fighter.Character.Alignment.Honor, 100, fighter.Character.Alignment.Dishonor, 0));
            }
        }

        private void AddLostedHonor(CharacterFighter fighter)
        {
            if (_fight.Type == FightTypeEnum.FIGHT_TYPE_AGRESSION)
            {
                fighter.Character.SubHonor(100);

                _resultAdditional.Add(new FightResultPvpData((byte)fighter.Character.Alignment.Grade, fighter.Character.Alignment.HonorGradeFloor,
                    fighter.Character.Alignment.HonorNextGradeFloor, fighter.Character.Alignment.Honor, -100, fighter.Character.Alignment.Dishonor, 0));
            }
        }
    }
}
