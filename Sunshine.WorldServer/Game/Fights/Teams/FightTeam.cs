﻿using Sunshine.Protocol.Messages;
using Sunshine.Protocol.Types;
using Sunshine.Protocol.Enums;
using Sunshine.WorldServer.Game.Actors;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Sunshine.WorldServer.Game.Actors.Fighters;

namespace Sunshine.WorldServer.Game.Fights.Teams
{
    public class FightTeam
    {
        public Fight Fight { get; set; }
        public List<FightActor> Attackers { get; set; }
        public List<FightActor> Defenders { get; set; }

        public FightTeam(Fight fight)
        {
            Fight = fight;
            Attackers = new List<FightActor>();
            Defenders = new List<FightActor>();
        }

        public void AddAttacker(FightActor attacker)
        {
            if(!Attackers.Contains(attacker))
                Attackers.Add(attacker);
            else
            {
                Attackers.Remove(attacker);
                Attackers.Add(attacker);
            }
        }

        public void AddDefender(FightActor defender)
        {
            if (!Defenders.Contains(defender))
                Defenders.Add(defender);
            else
            {
                Defenders.Remove(defender);
                Defenders.Add(defender);
            }
        }

        public bool IsFull(bool isAttackerTeam = false)
        {
            if (isAttackerTeam)
                return Attackers.Count >= 8 ? true : false;
            else
                return Defenders.Count >= 8 ? true : false;
        }

        public bool AreAllDead()
        {
            return Attackers.TrueForAll(x => x.IsDead) || Defenders.TrueForAll(x => x.IsDead);
        }

        public short BladePosition(bool isRedTeam = false)
        {
            switch(Fight.Type)
            {
                case FightTypeEnum.FIGHT_TYPE_PvM:
                    var monsterLeader = (MonsterFighter)Defenders.FirstOrDefault(x => (x as MonsterFighter).Monster.GetMonsterGroup != null);
                    var monsterCell = monsterLeader.Monster.GetMonsterGroup.GetEntityDisposition.cellId;                   
                    return isRedTeam ? (Attackers[0] as CharacterFighter).Character.Cell.Id
                                     : monsterCell >= 550 ? Fight.Map.Cells[monsterCell - 1].Id : Fight.Map.Cells[monsterCell + 1].Id;
                case FightTypeEnum.FIGHT_TYPE_AGRESSION:
                    return isRedTeam ? (Attackers[0] as CharacterFighter).Character.Cell.Id
                                     : (Defenders[0] as CharacterFighter).Character.Cell.Id;
                default:
                    return 0;
            }
        }

        public FightTeamInformations GetFightTeamInformations(bool isRedTeam = false)
        {
            if (isRedTeam)
                return new FightTeamInformations((sbyte)TeamEnum.TEAM_CHALLENGER, Attackers[0].Id, 0, (sbyte)Fight.Type, Attackers.Select(x => x.GetFightTeamMemberInformations));
            else
                return new FightTeamInformations((sbyte)TeamEnum.TEAM_DEFENDER, Defenders[0].Id, 1, (sbyte)Fight.Type, Defenders.Select(x => x.GetFightTeamMemberInformations));
        }
    }
}
