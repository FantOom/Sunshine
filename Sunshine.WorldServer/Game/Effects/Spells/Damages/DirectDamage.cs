﻿using Sunshine.Protocol.Enums;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Sunshine.WorldServer.Game.Effects.Spells.Damages
{
    [EffectHandler(EffectsEnum.Effect_DamageEarth), EffectHandler(EffectsEnum.Effect_DamageAir),
     EffectHandler(EffectsEnum.Effect_DamageFire), EffectHandler(EffectsEnum.Effect_DamageNeutral),
     EffectHandler(EffectsEnum.Effect_DamageWater)]
    public class DirectDamage : SpellEffectHandler
    {       
        public DirectDamage()
        {
        }

        public override void Apply()
        {
            var effectSchool = GetEffectSchool(Id);
            foreach(var actor in GetAffectedActors())
            {
                Damage damage = new Damage(effectSchool, DiceNum, DiceFace, Spell, Caster);
                actor.InflictDamage(damage);
            }
        }

        private EffectSchoolEnum GetEffectSchool(EffectsEnum effect)
        {
            EffectSchoolEnum result;
            switch (effect)
            {
                case EffectsEnum.Effect_DamageWater:
                case EffectsEnum.Effect_1014:
                    result = EffectSchoolEnum.Water;
                    break;
                case EffectsEnum.Effect_DamageEarth:
                case EffectsEnum.Effect_1016:
                    result = EffectSchoolEnum.Earth;
                    break;
                case EffectsEnum.Effect_DamageAir:
                case EffectsEnum.Effect_1013:
                    result = EffectSchoolEnum.Air;
                    break;
                case EffectsEnum.Effect_DamageFire:
                case EffectsEnum.Effect_1015:
                    result = EffectSchoolEnum.Fire;
                    break;
                case EffectsEnum.Effect_DamageNeutral:
                case EffectsEnum.Effect_1012:
                    result = EffectSchoolEnum.Neutral;
                    break;
                default:
                    throw new System.Exception(string.Format("Effect {0} has not associated School Type", effect));
            }
            return result;
        }
    }
}
