﻿using Sunshine.WorldServer.Client;
using Sunshine.WorldServer.Game.Spells;
using Sunshine.WorldServer.Game.Effects.Spells;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Sunshine.WorldServer.Game.Actors.Fighters;
using Sunshine.WorldServer.Game.Characters;
using Sunshine.WorldServer.Game.Effects.Items;

namespace Sunshine.WorldServer.Game.Effects
{
    public static class EffectDispatcher
    {
        public static void Dispatch(FightActor caster, Spell spell, Effect effect, short cell)
        {
            if (EffectManager.Instance.SpellEffects.ContainsKey(effect.Id))
            {
                SpellEffectHandler spellEffect = EffectManager.Instance.SpellEffects[effect.Id]();
                var affectedActors = EffectManager.Instance.GetAffectedActors(caster, effect, cell);
                spellEffect.Initialize(new List<object> { effect.Id, effect.DiceNum, effect.DiceFace, effect.Value,
                                                          effect.Delay, effect.Duration, effect.Target, cell, affectedActors, caster, spell });
            }
            else
                Logs.Logger.WriteError($"Cannot dispatch the effect {effect.Id} !");
        }
    }
}
