﻿using Sunshine.MySql.Database.Managers;
using Sunshine.MySql.Database.World.Monsters;
using Sunshine.Protocol.Types;
using Sunshine.Protocol.Utils;
using Sunshine.WorldServer.Game.Actors.Stats;
using Sunshine.WorldServer.Game.Fights;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Sunshine.WorldServer.Game.Actors.Monsters
{
    public class Monster
    {
        private MonsterGrade[] _grades;

        public MonsterTemplate Record { get; set; }

        public Monster(MonsterTemplate record, MonsterGrade[] grades)
        {
            _grades = grades;
            Record = record;
            Look = new ActorLook(this);
            Drops = MonsterManager.Instance.GetMonsterDrops(record.Id);              
            GenerateGrade();
        }

        public int Id { get; set; }

        public IEnumerable<MonsterDrop> Drops { get; set; }

        public ActorLook Look { get; set; }

        public MonsterGrade Grade { get; set; }

        public StatsFields Stats { get; set; }

        public MonsterGroup GetMonsterGroup { get; set; }

        public MonsterInGroupInformations GetMonsterInGroupInformations
            => new MonsterInGroupInformations(Record.Id, (sbyte)Grade.GradeId, Look.GetEntityLook());

        public void GenerateGrade()
        {
            AsyncRandom rdn = new AsyncRandom();
            Grade = _grades[rdn.Next(0, _grades.Length)];
        }

        public Monster Clone()
        {
            return (Monster)this.MemberwiseClone();
        }
    }
}
