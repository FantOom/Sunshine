﻿using Dapper;
using Sunshine.Mysql.Database;
using Sunshine.MySql.Database.World.Maps.Interactives;
using Sunshine.MySql.Database.World.Recipes;
using Sunshine.Protocol.Types;
using Sunshine.Protocol.Utils;
using System;
using Sunshine.Protocol.Utils.Extensions;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Sunshine.WorldServer.Game.Items;
using Sunshine.MySql.Database.World.Characters;
using Sunshine.MySql.Database.Managers;

namespace Sunshine.WorldServer.Game.Actors.Characters.Jobs
{
    public class JobManager : Singleton<JobManager>
    {
        public Dictionary<int, List<InteractiveSkill>> JobSkills = new Dictionary<int, List<InteractiveSkill>>();

        public Dictionary<int, List<RecipeTemplate>> Recipes = new Dictionary<int, List<RecipeTemplate>>();

        public Dictionary<int, List<HarvestTemplate>> Harvests = new Dictionary<int, List<HarvestTemplate>>();

        private Dictionary<sbyte, sbyte> _caseAvailable = new Dictionary<sbyte, sbyte>()
        {
            {1, 2}, {10, 3}, {20, 4}, {40, 5}, {60, 6}, {80, 7}, {100, 8}
        };

        public sbyte GetJobSlot(int level)
        {
            return _caseAvailable.LastOrDefault(x => x.Key <= level).Value;
        }

        public IEnumerable<SkillActionDescription> GetSkillActionDescription(sbyte job, sbyte level)
        {
            List<SkillActionDescription> skillActions = new List<SkillActionDescription>();
            foreach (var skill in JobSkills[job])
            {
                if (skill.GatheredRessourceItem != -1) // Harvest
                {
                    if (HasLevelToCollect(skill.Id, job, level))
                    {
                        var harvest = GetHarvest(skill.Id, job, level);
                        if (harvest != null)
                        {
                            var loot = harvest.Loot.Split(',');
                            skillActions.Add(new SkillActionDescriptionCollect((short)skill.Id, (byte)(harvest.Time / 100), byte.Parse(loot[0]), byte.Parse(loot[1])));
                        }
                    }
                }
                else // Craft
                    skillActions.Add(new SkillActionDescriptionCraft((short)skill.Id, GetJobSlot(level), 100));
            }
            return skillActions;
        }

        public Dictionary<int, List<RecipeTemplate>> GetAllRecipes()
        {
            Dictionary<int, List<RecipeTemplate>> recipes = new Dictionary<int, List<RecipeTemplate>>();            
            var recipesRecord =  DatabaseManager.Connection.Query<RecipeTemplate>($"SELECT * FROM recipes");
            foreach(var record in recipesRecord)
            {
                if (recipes.ContainsKey(record.Skill))
                    recipes[record.Skill].Add(record);
                else
                    recipes.Add(record.Skill, new List<RecipeTemplate> { record });
            }
            return recipes;
        }

        public Dictionary<int, List<HarvestTemplate>> GetAllHarvests()
        {
            Dictionary<int, List<HarvestTemplate>> harvests = new Dictionary<int, List<HarvestTemplate>>();
            var harvestsRecord = DatabaseManager.Connection.Query<HarvestTemplate>($"SELECT * FROM jobs_harvest");
            foreach (var record in harvestsRecord)
            {
                if (harvests.ContainsKey(record.Item))
                    harvests[record.Item].Add(record);
                else
                    harvests.Add(record.Item, new List<HarvestTemplate> { record });
            }
            return harvests;
        }

        public Tuple<RecipeTemplate, List<int>, List<int>> GetRecipe(IEnumerable<int> ingredients, int skill)
        {
            if (Recipes.ContainsKey(skill))
            {
                var ingredientsCSV = string.Join(",", ingredients.Select(x => x.ToString()));
                var recipe = Recipes[skill].FirstOrDefault(x => x.IngredientIdsCSV == ingredientsCSV);
                if (recipe != null)
                {
                    var ingredientsToList = recipe.IngredientIdsCSV.Split(',').Select(x => int.Parse(x)).ToList();
                    var quantitiesToList = recipe.QuantitiesCSV.Split(',').Select(x => int.Parse(x)).ToList();
                    return new Tuple<RecipeTemplate, List<int>, List<int>>(recipe, ingredientsToList, quantitiesToList);
                }
                else
                    return new Tuple<RecipeTemplate, List<int>, List<int>>(null, new List<int>(), new List<int>());
            }
            else
                return new Tuple<RecipeTemplate, List<int>, List<int>>(null, new List<int>(), new List<int>());
        }

        public HarvestTemplate GetHarvest(int skill, sbyte job, sbyte level)
        {
            var item = JobSkills[job].FirstOrDefault(x => x.Id == skill);
            if (item != null && item.GatheredRessourceItem != -1)
                return Harvests[item.GatheredRessourceItem].FirstOrDefault(x => x.Level == level);
            return null;
        }

        public bool HasLevelToCollect(int skill, CharacterJobRecord job, int level)
        {
            var item = JobSkills[job.Job].FirstOrDefault(x => x.Id == skill);
            if (item != null && item.GatheredRessourceItem != -1)
            {
                var ressource = Harvests[item.GatheredRessourceItem].FirstOrDefault();
                if (level < ressource.Level)
                    return false;
                return true;
            }
            return false;
        }

        public bool HasLevelToCollect(int skill, sbyte job, int level)
        {
            var item = JobSkills[job].FirstOrDefault(x => x.Id == skill);
            if (item != null && item.GatheredRessourceItem != -1)
            {
                var ressource = Harvests[item.GatheredRessourceItem].FirstOrDefault();
                if (level < ressource.Level)
                    return false;
                return true;
            }
            return false;
        }

        public double GetTimeToCollect(int skill, CharacterJobRecord job, int level)
        {
            var item = JobSkills[job.Job].FirstOrDefault(x => x.Id == skill);
            if (item != null && item.GatheredRessourceItem != -1)
                return Harvests[item.GatheredRessourceItem].LastOrDefault(x => x.Level <= level).Time;
            return 0;
        }

        public double GetTimeToCollect(int skill, sbyte job, int level)
        {
            var item = JobSkills[job].FirstOrDefault(x => x.Id == skill);
            if (item != null && item.GatheredRessourceItem != -1)
                return Harvests[item.GatheredRessourceItem].LastOrDefault(x => x.Level <= level).Time;
            return 0;
        }

        public int GetRandomQuantity(BasePlayerItem ressource, int level)
        {
            var harvest = Harvests[ressource.Template.Id].LastOrDefault(x => x.Level <= level);
            var quantites = harvest.Loot.Split(',').Select(x => int.Parse(x)).ToList();
            return new AsyncRandom().Next(quantites[0], quantites[1] + 1);
        }

        public long GetExperience(BasePlayerItem ressource)
        {
            return Harvests[ressource.Template.Id].First().Experience;
        }

        public long GetExperience(int recipeCount)
        {
            switch (recipeCount)
            {
                case 2:
                    return 25;
                case 3:
                    return 50;
                case 4:
                    return 75;
                case 5:
                    return 125;
                case 6:
                    return 250;
                case 7:
                    return 500;
                case 8:
                    return 750;
                default:
                    return 0;
            }
        }
    }
}
