﻿using Sunshine.MySql.Database.Managers;
using Sunshine.MySql.Database.World.Characters;
using Sunshine.Protocol.Enums;
using Sunshine.Protocol.Types;
using Sunshine.WorldServer.Game.Characters;
using Sunshine.WorldServer.Game.Spells;
using Sunshine.WorldServer.Handlers.Characters.Inventory;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Sunshine.WorldServer.Game.Actors.Characters.Spells
{
    public class SpellInventory
    {
        private Character _character;
        private List<CharacterSpellRecord> _spells;

        public SpellInventory(Character character)
        {
            _character = character;
            _spells = CharacterManager.Instance.GetCharacterSpells(character.Id);
        }

        public IEnumerable<CharacterSpellRecord> GetSpells()
        {
            return _spells;
        }

        public CharacterSpellRecord GetSpell(short spellId)
        {
            return _spells.FirstOrDefault(x => x.Spell == spellId);
        }

        public void LearnSpell(short spellId)
        {
            CharacterSpellRecord spell = new CharacterSpellRecord { OwnerId = _character.Id, Spell = spellId, Level = 1 };
            if(!this.HasSpell(spell))
            {
                _spells.Add(spell);
                InventoryHandler.SendSpellUpgradeSuccessMessage(_character.Client, spell.GetSpellItem());
            }
        }

        public void LearnSpell(CharacterSpellRecord spell)
        {
            if (!this.HasSpell(spell))
            {
                _spells.Add(spell);
                InventoryHandler.SendSpellUpgradeSuccessMessage(_character.Client, spell.GetSpellItem());
            }
        }

        public bool HasSpell(CharacterSpellRecord spell)
        {
            return this._spells.Contains(spell);
        }

        public bool CanBoostSpell(CharacterSpellRecord spell)
        {
            if (spell.Level >= 6)
                return false;
            if (spell.Level > _character.SpellsPoints)
                return false;
            if (_character.IsInFight)
                return false;
            return true;
        }

        public void BoostSpell(int spellId)
        {
            var spell = _spells.FirstOrDefault(x => x.Spell == spellId);
            if (spell != null)
            {
                if (!this.CanBoostSpell(spell))
                {
                    InventoryHandler.SendSpellUpgradeFailureMessage(_character.Client);
                    return;
                }
                else
                {
                    _spells.Remove(spell);
                    _character.SpellsPoints -= spell.Level;
                    spell.Level += 1;
                    _spells.Add(spell);
                    InventoryHandler.SendSpellUpgradeSuccessMessage(_character.Client, spell.GetSpellItem());
                    return;
                }
            }
            else
            {
                InventoryHandler.SendSpellUpgradeFailureMessage(_character.Client);
                return;
            }
        }
    }
}
