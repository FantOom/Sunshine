﻿using Sunshine.MySql.Database.Managers;
using Sunshine.MySql.Database.World.Characters;
using Sunshine.WorldServer.Game.Characters;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Sunshine.Protocol.Messages;
using Sunshine.WorldServer.Game.Actors.Npcs;
using Sunshine.WorldServer.Handlers.Actions;
using Sunshine.WorldServer.Client;
using Sunshine.Protocol.Enums;

namespace Sunshine.WorldServer.Game.Actors.Characters.Quests
{
    public class QuestsCollection
    {
        private Character _character;
        private Dictionary<short, CharacterQuestRecord> _quests;

        public QuestsCollection(Character character)
        {
            _character = character;
            _quests = CharacterManager.Instance.GetCharacterQuests(character.Id).ToDictionary(x => x.Quest);
        }

        public CharacterQuestRecord GetQuest(short id)
        {
            return _quests.ContainsKey(id) ? _quests[id] : null;
        }

        public IEnumerable<CharacterQuestRecord> GetQuests()
        {
            return _quests.Values;
        }

        public IEnumerable<CharacterQuestRecord> GetQuests(QuestTypeEnum type)
        {
            var questsObjectives = QuestManager.Instance.GetAllQuestObjectives().Values.Where(x => x.Type == type);
            return GetActivedQuests().Where(x => questsObjectives.Any(y => x.ObjectivesStartedCSV.Contains(y.Id.ToString())));
        }

        public IEnumerable<CharacterQuestRecord> GetFinishedQuests()
        {
            return _quests.Values.Where(x => x.IsFinished);
        }

        public IEnumerable<CharacterQuestRecord> GetActivedQuests()
        {
            return _quests.Values.Where(x => !x.IsFinished);
        }

        public bool HasQuest(short questId)
        {
            return _quests.ContainsKey(questId);
        }

        public IEnumerable<short> GetStepStartedObjectives(short questId)
        {
            var quest = _quests[questId];

            if (quest.ObjectivesStartedCSV == null || quest.ObjectivesStartedCSV == "")
                return new short[0];

            List<short> stepStartedObjectives = new List<short>();            
            string[] objectivesString = quest.ObjectivesStartedCSV.Split(';');
            foreach (var objString in objectivesString)
            {
                var splitObjs = objString.Split(',');
                foreach (var obj in splitObjs)
                    stepStartedObjectives.Add(short.Parse(obj));

            }
            return stepStartedObjectives;
        }

        public IEnumerable<short>GetStepStartedObjectives(short questId, short stepId)
        {
            var quest = _quests[questId];

            if (quest.ObjectivesStartedCSV == null || quest.ObjectivesStartedCSV == "")
                return new short[0];

            string[] objectivesString = quest.ObjectivesStartedCSV.Split(';');
            int indexStep = quest.StepsStartedCSV.Split(';').ToList().IndexOf(stepId.ToString());

            return objectivesString[indexStep].Split(',').Select(x => short.Parse(x));
        }

        public IEnumerable<short> GetStepEndedObjectives(short questId, short stepId)
        {
            var quest = _quests[questId];

            if (quest.ObjectivesEndedCSV == null || quest.ObjectivesEndedCSV == "")
                return new short[0];

            string[] objectivesString = quest.ObjectivesEndedCSV.Split(';');
            int indexStep = quest.ObjectivesEndedCSV.Split(';').ToList().IndexOf(stepId.ToString());

            return objectivesString[indexStep].Split(',').Select(x => short.Parse(x));
        }

        public IEnumerable<short> GetStepEndedObjectives(short questId)
        {
            var quest = _quests[questId];

            if (quest.ObjectivesEndedCSV == null || quest.ObjectivesEndedCSV == "")
                return new short[0];

            List<short> stepEndedObjectives = new List<short>();
            string[] objectivesString = quest.ObjectivesEndedCSV.Split(';');
            foreach (var objString in objectivesString)
            {
                var splitObjs = objString.Split(',');
                foreach (var obj in splitObjs)
                    stepEndedObjectives.Add(short.Parse(obj));

            }
            return stepEndedObjectives;
        }

        public void GetQuestStepInfos(short questId)
        {
            var quest = QuestManager.Instance.GetQuest(questId);
            if (quest != null)
            {
                var characterQuest = _character.Quests.GetQuest(questId);
                if (characterQuest != null)
                {
                    if (quest.StepIdsCSV == null || quest.StepIdsCSV == "")
                        _character.Client.Send(new QuestStepNoInfoMessage(questId));
                    else
                    {
                        bool parsed = short.TryParse(characterQuest.StepsStartedCSV.Split(';').LastOrDefault(), out short test);
                        if (parsed)
                        {
                            short step = short.Parse(characterQuest.StepsStartedCSV.Split(';').LastOrDefault());
                            var startedObjectives = _character.Quests.GetStepStartedObjectives(questId, step);
                            var endedObjectives = _character.Quests.GetStepEndedObjectives(questId, step);
                            _character.Client.Send(new QuestStepInfoMessage(questId, step, startedObjectives, startedObjectives.Select(x => !endedObjectives.Contains(x))));
                        }
                    }
                }
            }
        }

        public void AddQuest(int npc, short questId)
        {
            var quest = QuestManager.Instance.GetQuest(questId);
            if (quest != null)
            {
                var firstStep = short.Parse(quest.StepIdsCSV.Split(',')[0]);
                var allObjectives = QuestManager.Instance.GetQuestStep(firstStep).ObjectiveIdsCSV.Split(',');
                StringBuilder goodObjectives = new StringBuilder();
                foreach (var obj in allObjectives)
                {
                    if (obj == allObjectives.Last())
                        continue;

                    var objective = QuestManager.Instance.GetQuestObjective(short.Parse(obj));
                    if (QuestManager.Instance.ParseCriteria(objective.Criteria, _character))
                        goodObjectives.Append(obj + ",");
                }
                goodObjectives = goodObjectives.Remove(goodObjectives.Length - 1, 1);
                _quests.Add(questId, new CharacterQuestRecord { OwnerId = _character.Id, Quest = questId, StepsStartedCSV = firstStep.ToString(), ObjectivesStartedCSV = goodObjectives.ToString() });
                var npcsIds = _character.Map.RolePlayActors.Where(x => x is Npc && (x as Npc).Record.HasQuest).Select(x => (x as Npc).Id);
                _character.SendInformationMessage(TextInformationTypeEnum.TEXT_INFORMATION_MESSAGE, 54, new object[] { questId });
                _character.Client.Send(new QuestStartedMessage((ushort)questId));
                _character.Client.Send(new MapNpcsQuestStatusUpdateMessage(_character.Map.Id, npcsIds.Where(x => x != npc), npcsIds));
            }
        }

        public void UpdateObjective(short questId, string objectiveId)
        {
            var quest = _quests[questId];
            var startedObjs = quest.ObjectivesStartedCSV.Split(';').ToList();
            var indexStep = startedObjs.IndexOf(objectiveId);
            var indexObj = startedObjs[indexStep].IndexOf(objectiveId);

            if (quest.ObjectivesStartedCSV.Length > indexObj + 1 &&
                quest.ObjectivesStartedCSV[indexObj] + 1 == ',')
                quest.ObjectivesStartedCSV.Split(';').ToList()[indexStep] = startedObjs[indexStep].Remove(indexStep, objectiveId.Length + 1);
            else
                quest.ObjectivesStartedCSV.Split(';').ToList()[indexStep] = startedObjs[indexStep].Remove(indexStep, objectiveId.Length);

            if (quest.ObjectivesEndedCSV == null || quest.ObjectivesEndedCSV == "")
                quest.ObjectivesEndedCSV += objectiveId;
            else
                quest.ObjectivesEndedCSV.Split(';').ToList()[indexStep] += ("," + objectiveId);

            var questObj = QuestManager.Instance.GetQuestObjective(short.Parse(objectiveId));
            var stepIdsCSV = QuestManager.Instance.GetQuest(questId).StepIdsCSV.Split(',');
            var objectiveIdsCSV = QuestManager.Instance.GetQuestStep(questObj.Step).ObjectiveIdsCSV.Split(',');
            string stepToString = questObj.Step.ToString();

            if (stepIdsCSV.Last() == stepToString && objectiveIdsCSV.Last() == objectiveId)
                this.EndStep(quest, stepToString, true);
            else if (objectiveIdsCSV.Last() == objectiveId)
                this.EndStep(quest, stepToString);
            else
            {
                _quests[questId] = quest;
                _character.Client.Send(new QuestObjectiveValidatedMessage((ushort)questId, ushort.Parse(objectiveId)));
            }
        }

        private void EndStep(CharacterQuestRecord quest, string stepToString, bool isFinsihed = false)
        {
            var indexStepCSV = quest.StepsStartedCSV.IndexOf(stepToString);

            if (quest.StepsStartedCSV.Length > indexStepCSV + 1 &&
                quest.StepsStartedCSV[indexStepCSV] + 1 == ',')
                quest.StepsStartedCSV = quest.StepsStartedCSV.Remove(indexStepCSV, stepToString.Length + 1);
            else
                quest.StepsStartedCSV = quest.StepsStartedCSV.Remove(indexStepCSV, stepToString.Length);

            if (quest.StepsEndedCSV == null || quest.StepsEndedCSV == "")
                quest.StepsEndedCSV += stepToString;
            else
                quest.StepsEndedCSV.Split(';').ToList()[indexStepCSV] += ("," + stepToString);

            if (!isFinsihed)
                _character.Client.Send(new QuestStepValidatedMessage((ushort)quest.Quest, ushort.Parse(stepToString)));
            else
                _character.Client.Send(new QuestValidatedMessage((ushort)quest.Quest));

            _quests[quest.Quest] = quest;
        }
    }
}
