﻿using Sunshine.MySql.Database.Managers;
using Sunshine.MySql.Database.World.Characters.Shortcuts;
using Sunshine.Protocol.Enums;
using Sunshine.WorldServer.Game.Characters;
using Sunshine.WorldServer.Handlers.Characters.Shorcuts;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Sunshine.WorldServer.Game.Actors.Characters.Shortcuts
{
    public class ShortcutBar
    {
        private Character _character;
        public List<SpellShortcut> SpellShortcuts { get; set; }
        public List<ItemShortcut> ItemShortcuts { get; set; }
        
        public ShortcutBar(Character character)
        {
            _character = character;
            SpellShortcuts = CharacterManager.Instance.GetShortcuts<SpellShortcut>(character.Id);
            ItemShortcuts = CharacterManager.Instance.GetShortcuts<ItemShortcut>(character.Id);
        }

        public IEnumerable<Protocol.Types.Shortcut> GetShortcuts(ShortcutBarEnum barType)
        {
            switch (barType)
            {
                case ShortcutBarEnum.SPELL:
                    return SpellShortcuts.Select(x => x.GetNetworkShortcut());

                case ShortcutBarEnum.OBJECT:
                    return ItemShortcuts.Select(x => x.GetNetworkShortcut());

                default:
                    return null;
            }
        }   

        public Shortcut GetShortcut(ShortcutBarEnum barType, int slot)
        {
            switch (barType)
            {
                case ShortcutBarEnum.SPELL:
                    return SpellShortcuts.FirstOrDefault(x => x.Slot == slot);

                case ShortcutBarEnum.OBJECT:
                    return ItemShortcuts.FirstOrDefault(x => x.Slot == slot);

                default:
                    return null;
            }
        }

        public void AddShortcut(Protocol.Types.Shortcut shortcut, ShortcutBarEnum barType)
        {
            if (!this.IsSlotFree(shortcut.slot, barType))
                this.RemoveShortcut(barType, shortcut.slot);

            if (barType == ShortcutBarEnum.SPELL)
            {
                SpellShortcut spellShortcut = new SpellShortcut
                {
                    OwnerId = _character.Id,
                    Spell = (shortcut as Protocol.Types.ShortcutSpell).spellId,
                    Slot = (shortcut as Protocol.Types.ShortcutSpell).slot
                };

                this.SpellShortcuts.Add(spellShortcut);
                ShortcutHandler.SendShortcutBarRefreshMessage(this._character.Client, barType, spellShortcut);
            }
            else
            {
                ItemShortcut itemShortcut = new ItemShortcut
                {
                    OwnerId = _character.Id,
                    ItemUid = (shortcut as Protocol.Types.ShortcutObjectItem).itemUID,
                    ItemGid = (shortcut as Protocol.Types.ShortcutObjectItem).itemGID,
                    Slot = (shortcut as Protocol.Types.ShortcutObjectItem).slot
                };

                this.ItemShortcuts.Add(itemShortcut);
                ShortcutHandler.SendShortcutBarRefreshMessage(this._character.Client, barType, itemShortcut);
            }
        }

        public void RemoveShortcut(ShortcutBarEnum barType, int slot)
        {
            Shortcut shortcut = this.GetShortcut(barType, slot);
            if (shortcut != null)
            {
                if (barType == ShortcutBarEnum.SPELL)
                    this.SpellShortcuts.Remove((SpellShortcut)shortcut);
                else
                    this.ItemShortcuts.Remove((ItemShortcut)shortcut);
                ShortcutHandler.SendShortcutBarRemovedMessage(this._character.Client, barType, slot);
            }
        }

        public void SwitchShortcut(ShortcutBarEnum barType, int slot, int newSlot)
        {
            Shortcut shortcut = this.GetShortcut(barType, slot);
            Shortcut newShortcut = this.GetShortcut(barType, newSlot);
            dynamic shortcutType = null;

            if (!this.IsSlotFree(slot, barType))
            {
                this.RemoveShortcut(barType, slot);
                this.RemoveShortcut(barType, newSlot);

                if (newShortcut != null)
                {
                    if (newShortcut is SpellShortcut)
                        shortcutType = new Protocol.Types.ShortcutSpell(slot, (newShortcut as SpellShortcut).Spell);
                    else
                        shortcutType = new Protocol.Types.ShortcutObjectItem(slot, (newShortcut as ItemShortcut).ItemUid, 
                                                                            (newShortcut as ItemShortcut).ItemGid);

                    this.AddShortcut((Protocol.Types.Shortcut)shortcutType, barType);
                }
                else
                    ShortcutHandler.SendShortcutBarRemovedMessage(this._character.Client, barType, slot);

                if (shortcut is SpellShortcut)
                    shortcutType = new Protocol.Types.ShortcutSpell(newSlot, (shortcut as SpellShortcut).Spell);
                else
                    shortcutType = new Protocol.Types.ShortcutObjectItem(newSlot, (shortcut as ItemShortcut).ItemUid,
                                                                        (shortcut as ItemShortcut).ItemGid);

                this.AddShortcut((Protocol.Types.Shortcut)shortcutType, barType);
            }
        }       
        
        public void AddNextShortcuts(ShortcutBarEnum barType)
        {
            int nextFreeSlot = 0;
            if(barType == ShortcutBarEnum.SPELL)
            {
                var spells = _character.Spells.GetSpells().Where(x => x.Level <= _character.Level);
                var spellShorcuts = GetShortcuts(barType).ToDictionary(entry => (entry as Protocol.Types.ShortcutSpell).spellId);
                foreach(var spell in spells)
                {
                    if(spellShorcuts.ContainsKey(spell.Spell))
                        continue;
                    else
                    {
                        nextFreeSlot = GetNextFreeSlot(barType);
                        AddShortcut(new Protocol.Types.ShortcutSpell(nextFreeSlot, spell.Spell), barType);
                    }
                }
            }
        }

        public bool IsSlotFree(int slot, ShortcutBarEnum barType)
        {
            bool result;
            if (barType == ShortcutBarEnum.SPELL)
            {
                result = this.SpellShortcuts.FirstOrDefault(x => x.Slot == slot) != null ? false : true;
            }
            else
            {
                if (barType != ShortcutBarEnum.OBJECT)
                    result = false;

                else if (this.ItemShortcuts.FirstOrDefault(x => x.Slot == slot) == null)
                    result = true;
                else
                    result = false;
            }
            return result;
        }

        public int GetNextFreeSlot(ShortcutBarEnum barType)
        {
            int result;
            for (int i = 0; i < 40; i++)
            {
                if (this.IsSlotFree(i, barType))
                {
                    result = i;
                    return result;
                }
            }
            result = 40;
            return result;
        }
    }
}

    

