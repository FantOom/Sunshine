﻿using Sunshine.MySql.Database.Managers;
using Sunshine.WorldServer.Handlers.Dialogs;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Sunshine.WorldServer.Game.Actors.Npcs.Replies
{
    [ReplyHandler(2)]
    public class TeleportReply : Reply
    {
        public TeleportReply()
        {
        }

        public override bool Execute()
        {
            var param = Parameters[0].ToString().Split(',');
            var map = int.Parse(param[0]);
            var cell = short.Parse(param[1]);
            if (MapManager.Instance.GetMap(map) != null)
            {
                Client.Character.Teleport(map, cell);
                Client.Character.Dialog = null;
                DialogHandler.SendLeaveDialogMessage(Client);
                return true;
            }
            return false;
        }
    }
}
