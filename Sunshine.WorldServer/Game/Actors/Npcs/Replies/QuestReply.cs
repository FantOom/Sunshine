﻿using Sunshine.Protocol.Messages;
using Sunshine.WorldServer.Handlers.Dialogs;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Sunshine.WorldServer.Game.Actors.Npcs.Replies
{
    [ReplyHandler(5)]
    public class QuestReply : Reply
    {
        public QuestReply()
        {
        }

        public override bool Execute()
        {
            var quests = Parameters[0] as List<string>;
            short quest = short.Parse(quests[0]);
            if (!Client.Character.Quests.HasQuest(quest))
            {
                Client.Character.Quests.AddQuest(Npc.Id, quest);
                Client.Character.Dialog = null;
                DialogHandler.SendLeaveDialogMessage(Client);
                return true;
            }
            else
            {
                Client.Character.SendServerMessage("Tu as déjà eu cette quête.");
                return false;
            }
        }
    }
}
