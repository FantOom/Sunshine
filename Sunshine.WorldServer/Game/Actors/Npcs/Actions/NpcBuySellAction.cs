﻿using Sunshine.MySql.Database.Managers;
using Sunshine.MySql.Database.World.Npcs;
using Sunshine.Protocol.Enums;
using Sunshine.WorldServer.Game.Characters;
using Sunshine.WorldServer.Game.Items;
using Sunshine.WorldServer.Handlers.Basic;
using Sunshine.WorldServer.Handlers.Characters.Inventory;
using Sunshine.WorldServer.Handlers.Context.Roleplay;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Sunshine.WorldServer.Game.Actors.Npcs.Actions
{
    public class NpcBuySellAction : NpcAction
    {
        private Character _character;
        private Npc _npc;

        public NpcBuySellAction(Npc npc, Character character)
        {
            _npc = npc;
            _character = character;
        }

        public override void Execute()
        {
            _character.Dialog = this;
            InventoryHandler.SendExchangeStartOkNpcShopMessage(_character.Client, _npc);
        }

        public void BuyItem(int objectGid, int quantity)
        {
            if (quantity <= 0 || !_npc.Shops.ContainsKey(objectGid))
            {
                InventoryHandler.SendExchangeErrorMessage(_character.Client, ExchangeErrorEnum.BUY_ERROR);
                return;
            }

            var npc = _npc.Shops[objectGid];

            var newItem = ItemManager.Instance.CreatePlayerItem(objectGid, quantity);

            if (!CanBuy(npc, newItem, quantity))
            {
                InventoryHandler.SendExchangeErrorMessage(_character.Client, ExchangeErrorEnum.BUY_ERROR);
                return;
            }

            BasicHandler.SendTextInformationMessage(_character.Client, TextInformationTypeEnum.TEXT_INFORMATION_MESSAGE, 21, new object[] { quantity, objectGid });

            _character.Inventory.AddItem(newItem, quantity);

            if (_npc.Record.Token != 0)
                _character.Inventory.RemoveItem(_character.Inventory.GetItem(_npc.Record.Token), (int)(newItem.Template.Price * quantity));
            else
                _character.Inventory.SetKamas(-(int)(newItem.Template.Price * quantity));

            BasicHandler.SendTextInformationMessage(_character.Client, TextInformationTypeEnum.TEXT_INFORMATION_MESSAGE, 46, new object[] { newItem.Template.Price * quantity });

            InventoryHandler.SendExchangeBuyOkMessage(_character.Client);
        }

        public bool CanBuy(NpcShop npc, BasePlayerItem item, int quantity)
        {
            if(_npc.Record.Token != 0)
            {
                var tryItem = _character.Inventory.GetItem(_npc.Record.Token);
                if (tryItem == null || tryItem.Stack < (item.Template.Price * quantity))
                    return false;
                return true;
            }
            else
            {

                if (_character.Inventory.Kamas < (item.Template.Price * quantity))
                    return false;
                return true;
            }
        }
    }
}
