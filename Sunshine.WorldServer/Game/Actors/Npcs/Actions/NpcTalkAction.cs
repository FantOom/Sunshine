﻿using Sunshine.MySql.Database.Managers;
using Sunshine.WorldServer.Game.Actors.Npcs.Replies;
using Sunshine.WorldServer.Game.Characters;
using Sunshine.WorldServer.Handlers.Context.Roleplay;
using Sunshine.WorldServer.Handlers.Dialogs;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Sunshine.WorldServer.Game.Actors.Npcs.Actions
{
    public class NpcTalkAction : NpcAction
    {
        private Character _character;
        private Npc _npc;

        public NpcTalkAction(Npc npc, Character character)
        {
            _npc = npc;
            _character = character;
        }

        public override void Execute()
        {
            _character.Dialog = this;
            ContextRoleplayHandler.SendNpcDialogCreationMessage(_character.Client, _npc);
            ContextRoleplayHandler.SendNpcDialogQuestionMessage(_character.Client, _npc);
        }

        public void ChangeMessage(short reply)
        {
            var types = _npc.GetNpcTypes;
            var messages = _npc.GetDialogMessagesId;
            var replies = _npc.GetDialogRepliesId;

            var replie = replies.FirstOrDefault(x => x.Contains(reply.ToString()));
            int index = replies.IndexOf(replie);
            int indexType = replie.IndexOf(reply.ToString());

            if (messages.Count >= index + 1)
            {
                if (types[index][indexType] != "1")
                {
                    if (!ReplyDispatcher.Dispatch(_character.Client, _npc, new List<object> { byte.Parse(types[index][indexType]), _npc.GetParameters.Count > 0 ? _npc.GetParameters[index] : new List<string>()}))
                        return;
                }
                else
                {                   
                    index++;
                    if (messages.Count <= index)
                    {
                        _character.Dialog = null;
                        DialogHandler.SendLeaveDialogMessage(_character.Client);
                    }
                    else
                        ContextRoleplayHandler.SendNpcDialogQuestionMessage(_character.Client, messages[index][0],
                                                new string[0], replies[index].Select(x => short.Parse(x)));
                }
            }
            else
            {
                _character.Dialog = null;
                DialogHandler.SendLeaveDialogMessage(_character.Client);
            }
        }
    }
}
