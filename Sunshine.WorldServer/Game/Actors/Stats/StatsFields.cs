﻿using Sunshine.MySql.Database.Managers;
using Sunshine.MySql.Database.World.Characters;
using Sunshine.MySql.Database.World.Monsters;
using Sunshine.Protocol.Enums;
using Sunshine.Protocol.Types;
using Sunshine.WorldServer.Game.Actors.Monsters;
using Sunshine.WorldServer.Game.Characters;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Sunshine.WorldServer.Game.Actors.Stats
{
    public class StatsFields
    {
        public const int APLimit = 12;

        public const int MPLimit = 6;

        public const int RangeLimit = 6;

        private Dictionary<StatsEnum, StatsData> _stats;

        private object _actor;

        public CharacterStatsRecord Record { get; set; }

        public StatsFields(object actor)
        {
            _stats = new Dictionary<StatsEnum, StatsData>();
            _actor = actor;
            foreach (StatsEnum stats in typeof(StatsEnum).GetEnumValues())
            {
                if(stats == StatsEnum.Health)
                    _stats.Add(stats, new StatsHealth(actor, 0));
                else
                    _stats.Add(stats, new StatsData(0, null));
            }

            if (actor is Character)
                Initialize((actor as Character));
            else
                Initialize((actor as Monster));
        }

        public StatsData this[StatsEnum stat]
        {
            get
            {
                if (_stats.ContainsKey(stat))
                    return _stats[stat];
                else
                    return null;
            }
        }

        public StatsData AP
        {
            get { return this[StatsEnum.AP]; }
        }

        public StatsData MP
        {
            get { return this[StatsEnum.MP]; }
        }

        public StatsHealth Health
        {
            get { return (StatsHealth)this[StatsEnum.Health]; }
        }

        public StatsData Prospecting
        {
            get { return this[StatsEnum.Prospecting]; }
        }

        public StatsData Initiative
        {
            get { return this[StatsEnum.Initiative]; }
        }

        private void Initialize(Character character)
        {
            Record = CharacterManager.Instance.GetCharacterStats(character.Id);
            this[StatsEnum.AP].Base = Record.AP;
            this[StatsEnum.MP].Base = Record.MP;
            this[StatsEnum.Vitality].Base = Record.Vitality;
            this[StatsEnum.Health].Base = Record.Health;
            (this[StatsEnum.Health] as StatsHealth).Taken = Record.DamageTaken;
            this[StatsEnum.Strength].Base = Record.Strength;
            this[StatsEnum.Chance].Base = Record.Chance;
            this[StatsEnum.Wisdom].Base = Record.Wisdom;
            this[StatsEnum.Intelligence].Base = Record.Intelligence;
            this[StatsEnum.Agility].Base = Record.Agility;
            this[StatsEnum.SummonLimit].Base = 1;
            this[StatsEnum.Initiative].Base = (short)GetInitiative(character.Level);
            this[StatsEnum.Prospecting].Base = (short)(100 + (this[StatsEnum.Chance].Total / 10));
            this[StatsEnum.DodgeAPProbability].Base = (short)(Record.Wisdom / 10);
            this[StatsEnum.DodgeMPProbability].Base = (short)(Record.Wisdom / 10);
            this[StatsEnum.AP].Limit = APLimit;
            this[StatsEnum.MP].Limit = MPLimit;
            this[StatsEnum.Range].Limit = RangeLimit;
        }

        private void Initialize(Monster monster)
        {
            var Record = monster.Grade;
            this[StatsEnum.AP].Base = Record.ActionPoints;
            this[StatsEnum.MP].Base = Record.MovementPoints;
            this[StatsEnum.Vitality].Base = Record.Vitality;
            this[StatsEnum.Health].Base = Record.LifePoints;
            this[StatsEnum.Strength].Base = Record.Strength;
            this[StatsEnum.Chance].Base = Record.Chance;
            this[StatsEnum.Wisdom].Base = Record.Wisdom;
            this[StatsEnum.Intelligence].Base = Record.Intelligence;
            this[StatsEnum.Agility].Base = Record.Agility;
            this[StatsEnum.Initiative].Base = (short)Record.Initiative;
            this[StatsEnum.EarthResistPercent].Base = Record.EarthResistance;
            this[StatsEnum.WaterResistPercent].Base = Record.WaterResistance;
            this[StatsEnum.FireResistPercent].Base = Record.FireResistance;
            this[StatsEnum.NeutralResistPercent].Base = Record.NeutralResistance;
            this[StatsEnum.AirResistPercent].Base = Record.AirResistance;
            this[StatsEnum.DodgeAPProbability].Base = Record.PaDodge;
            this[StatsEnum.DodgeMPProbability].Base = Record.PmDodge;
        }

        private int GetInitiative(short level)
        {
           return this[StatsEnum.Strength].Total + this[StatsEnum.Intelligence].Total + this[StatsEnum.Agility].Total
                + this[StatsEnum.Chance].Total * (this[StatsEnum.Health].Base / (50 + (level * 5) + this[StatsEnum.Vitality].Total));           
        }

        public void Update()
        {
            Record.AP = this[StatsEnum.AP].Base;
            Record.MP = this[StatsEnum.MP].Base;
            Record.Strength = this[StatsEnum.Strength].Base;
            Record.Intelligence = this[StatsEnum.Intelligence].Base;
            Record.Wisdom = this[StatsEnum.Wisdom].Base;
            Record.Vitality = this[StatsEnum.Vitality].Base;
            Record.Agility = this[StatsEnum.Agility].Base;
            Record.Chance = this[StatsEnum.Chance].Base;
            Record.Health = this[StatsEnum.Health].Base;
            Record.DamageTaken = (this[StatsEnum.Health] as StatsHealth).Taken;
        }

        public StatsFields Clone()
        {
            return (StatsFields)this.MemberwiseClone();
        }
    }
}
