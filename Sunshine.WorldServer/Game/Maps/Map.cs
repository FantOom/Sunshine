﻿using Sunshine.MySql.Database.Managers;
using Sunshine.MySql.Database.World;
using Sunshine.MySql.Database.World.Maps;
using Sunshine.Protocol.Enums;
using Sunshine.Protocol.Tools.D2p;
using Sunshine.Protocol.Tools.Dlm;
using Sunshine.Protocol.Types;
using Sunshine.WorldServer.Client;
using Sunshine.WorldServer.Game.Actors;
using Sunshine.WorldServer.Game.Characters;
using Sunshine.WorldServer.Game.Fights;
using Sunshine.WorldServer.Game.Maps.Dungeons;
using Sunshine.WorldServer.Game.Maps.Interactives;
using Sunshine.WorldServer.Game.Maps.Triggers;
using Sunshine.WorldServer.Handlers;
using Sunshine.WorldServer.Handlers.Context;
using Sunshine.WorldServer.Handlers.Context.Roleplay;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Sunshine.WorldServer.Game.Maps
{
    public class Map
    {
        public MapRecord Record { get; set; }

        public DlmCellData[] Cells { get; set; }
          
        public List<WorldClient> Clients;

        public List<RolePlayActor> RolePlayActors { get; set; }

        public List<short> BlueCells { get; set; }

        public List<short> RedCells { get; set; }

        public List<Element> Elements { get; set; }

        public List<Interactive> Interactives { get; set; }

        public List<Trigger> Triggers { get; set; }

        public Dungeon Dungeon { get; set; }

        public List<Fight> Fights { get; set; }

        public bool AllowChallenge { get { return !(BlueCells.Count <= 0 && RedCells.Count <= 0); } }

        public Map(MapRecord record)
        {
            Record = record;
            Cells = new DlmCellData[560];      
            Clients = new List<WorldClient>();
            RolePlayActors = new List<RolePlayActor>();
            Fights = new List<Fight>();
        }

        public int Id
            => Record.Id;

        public int SubAreaId
            => Record.SubAreaId;

        public int LeftNeighbourId
            => Record.LeftNeighbourId;

        public int RightNeighbourId
            => Record.RightNeighbourId;

        public int TopNeighbourId
            => Record.TopNeighbourId;

        public int BottomNeighbourId
            => Record.BottomNeighbourId;

        public bool IsDungeon { get { return Dungeon != null; } }

        public void EnterActor(RolePlayActor actor)
        {
            lock (actor)
            {
                if (actor is Character)
                {
                    var client = (actor as Character).Client;

                    if (!Clients.Contains(client))
                        Clients.Add(client);

                    if (Interactives.Any(x => x.Type == 16) && !client.Character.HasZaap(Id))
                        client.Character.DiscoverZaap(this);
                   
                    foreach (var actorClient in Clients)
                    {
                        ContextRoleplayHandler.SendGameRolePlayShowActorMessage(client, actorClient.Character);
                        if (actorClient != client)
                            ContextRoleplayHandler.SendGameRolePlayShowActorMessage(actorClient, actor);
                    }
                }
                else
                    Clients.ForEach(x => ContextRoleplayHandler.SendGameRolePlayShowActorMessage(x, actor));
            }
        }

        public void LeaveActor(RolePlayActor actor)
        {
            lock (actor)
            {
                if (actor is Character)
                {
                    var client = (actor as Character).Client;
                    Clients.Remove(client);
                }
                Clients.ForEach(x => ContextHandler.SendGameContextRemoveElementMessage(x, actor));
            }
        }

        public void Refresh(RolePlayActor actor, bool isRemoved = false)
        {
            lock (actor)
            {
                if (isRemoved)
                    Clients.ForEach(x => ContextHandler.SendGameContextRemoveElementMessage(x, actor));
                else
                    Clients.ForEach(x => ContextRoleplayHandler.SendGameRolePlayShowActorMessage(x, actor));
            }
        }

        public RolePlayActor GetActor(int id)
        {
            return RolePlayActors.FirstOrDefault(x => x.Id == id);
        }

        public void GeneratePatternCells()
        {
            if (Cells.Length <= 0)
                return;

            var blueCells = MapManager.Instance.GetPatternCells(this, true);
            var redCells = MapManager.Instance.GetPatternCells(this, false);
          
            if (blueCells.Count > 0 && redCells.Count > 0)
            {
                BlueCells = blueCells;
                RedCells = redCells;
            }
            else
            {
                foreach (var pattern in MapManager.Instance.cellsPattern)
                {
                    blueCells = new List<short>(pattern.Value[0]);
                    redCells = new List<short>(pattern.Value[1]);

                    // Direction
                    blueCells.RemoveAt(blueCells.Count - 1);
                    redCells.RemoveAt(redCells.Count - 1);

                    if(blueCells.TrueForAll(x => Cells[x].Walkable == true) && 
                       redCells.TrueForAll(x => Cells[x].Walkable == true))
                    {
                        BlueCells = pattern.Value[0];
                        RedCells = pattern.Value[1];
                        break;
                    }
                }               
            }
        }
    }
}
