﻿using Sunshine.Protocol.Enums;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Sunshine.WorldServer.Game.Maps
{
    public class ObjectPosition
    {
        public ObjectPosition(short cell, DirectionsEnum direction)
        {
            Cell = cell;
            Direction = direction;
        }

        public short Cell { get; set; }

        public DirectionsEnum Direction { get; set; }

        public MapPoint Point { get { return new MapPoint(Cell); } }
    }
}
