﻿using Sunshine.MySql.Database.Managers;
using Sunshine.WorldServer.Client;
using Sunshine.WorldServer.Game.Maps.Interactives.Skills;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Sunshine.WorldServer.Game.Maps.Interactives
{
    public static class SkillDispatcher
    {
        public static void Dispatch(WorldClient client, int skillUid, int elementId)
        {
            if (InteractiveManager.Instance.Interactives.ContainsKey(skillUid))
            {
                var skillId = InteractiveManager.Instance.Interactives[skillUid];

                if (client.Character.Map.Id != skillId.Item1)
                    return;

                if (SkillManager.Instance.Skills.ContainsKey(skillId.Item2))
                {
                    var skill = SkillManager.Instance.Skills[skillId.Item2]();
                    skill.Initialize(client, skillId.Item2, elementId);
                }
                else
                    Logs.Logger.WriteError($"Cannot dispatch the skill {skillId} !");
            }
        }
    }
}
