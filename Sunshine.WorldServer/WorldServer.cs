﻿using Sunshine.BaseClient;
using Sunshine.Logs;
using Sunshine.Protocol.IO;
using Sunshine.Protocol.Messages;
using Sunshine.Servers;
using Sunshine.WorldServer.Client;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Sockets;
using System.Text;
using System.Threading.Tasks;

namespace Sunshine.WorldServer
{
    public class WorldServer
    {
        #region World Config
        private static string[] m_config = File.ReadAllLines(".\\Config.xml");
        public static string Ip { get { return m_config[9].Remove(0, 5); } }
        public static int Port { get { return int.Parse(m_config[10].Remove(0, 7)); } }
        private Socket m_worldserver;
        private Socket m_client;
        public Dictionary<Socket, IBaseClient> worldClients = new Dictionary<Socket, IBaseClient>();
        private IPEndPoint World_Address
        {
            get { return new IPEndPoint(IPAddress.Parse(Ip), Port); }
        }
        #endregion

       

        public WorldServer()
        {
            m_worldserver = new Socket(AddressFamily.InterNetwork, SocketType.Stream, ProtocolType.Tcp);
        }

        public void Initialize()
        {
            Bind();
            Listen(100);
            Accept();
        }

        private void Bind()
        {
            m_worldserver.Bind(World_Address);
        }

        private void Listen(int file)
        {
            m_worldserver.Listen(file);
            Logger.WriteInfo(string.Format("Starting IPC World {0}:{1}", Ip, Port));
        }

        private void Accept()
        {
            m_worldserver.BeginAccept(new AsyncCallback(AcceptCallBack), null);
        }       

        private void AcceptCallBack(IAsyncResult result)
        {
            WorldClient worldClient = new WorldClient(m_client = m_worldserver.EndAccept(result));
            try
            {
                ServersManager.Instance.AddClient(m_client, worldClient);
                Logger.WriteInfo(string.Format("Client <{0}:{1}> connected to WorldServer...", worldClient.Ip, worldClient.Port));
                worldClient.Send(new ProtocolRequired(1375, 1375));
                worldClient.Send(new HelloGameMessage());
                worldClient.Initialize();
                Accept();
            }
            catch (Exception e)
            {
                Logger.WriteError(e.ToString());
                worldClient.Disconnect();
                return;
            }
        }
    }
}
