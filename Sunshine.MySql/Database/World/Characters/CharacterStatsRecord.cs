﻿using Dapper.Contrib.Extensions;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Sunshine.MySql.Database.World.Characters
{
    [Table("characters_stats")]
    public class CharacterStatsRecord
    {
        public int OwnerId { get; set; }
        public short AP { get; set; }
        public short MP { get; set; }
        public short Health { get; set; }
        public short Vitality { get; set; }
        public short Strength { get; set; }
        public short Chance { get; set; }
        public short Wisdom { get; set; }
        public short Intelligence { get; set; }
        public short Agility { get; set; }
        public short DamageTaken { get; set; }
    }
}
