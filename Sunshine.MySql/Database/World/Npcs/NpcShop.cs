﻿using Dapper.Contrib.Extensions;
using Sunshine.Protocol.Types;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Sunshine.MySql.Database.World.Npcs
{
    [Table("npcs_items")]
    public class NpcShop
    {
        public int NpcId { get; set; }
        public int Item { get; set; }
    }
}
