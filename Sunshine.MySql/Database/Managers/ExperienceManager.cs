﻿using Sunshine.MySql.Database.World.Experiences;
using System;
using Dapper;
using Dapper.Contrib.Extensions;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Sunshine.Mysql.Database;
using Sunshine.Protocol.Utils;

namespace Sunshine.MySql.Database.Managers
{
    public class ExperienceManager : Singleton<ExperienceManager>
    {
        private Dictionary<int, Experience> _experiencesByLevel = new Dictionary<int, Experience>();

        private Dictionary<long, Experience> _experiencesByCharacterExp = new Dictionary<long, Experience>();

        private Dictionary<long, Experience> _experiencesByGuildExp = new Dictionary<long, Experience>();

        private Dictionary<long, Experience> _experiencesByMountExp = new Dictionary<long, Experience>();

        private Dictionary<int, Experience> _experiencesByAlignmentExp = new Dictionary<int, Experience>();

        private Dictionary<long, Experience> _experiencesByJobExp = new Dictionary<long, Experience>();

        public void LoadAllExperiences()
        {
            _experiencesByLevel = DatabaseManager.Connection.Query<Experience>("SELECT * FROM experiences").ToDictionary(x => x.Level);
            _experiencesByCharacterExp = DatabaseManager.Connection.Query<Experience>("SELECT * FROM experiences").ToDictionary(x => x.CharacterExp);
            _experiencesByGuildExp = DatabaseManager.Connection.Query<Experience>("SELECT * FROM experiences").ToDictionary(x => x.GuildExp);
            _experiencesByMountExp = DatabaseManager.Connection.Query<Experience>("SELECT * FROM experiences WHERE Level <= '100'").ToDictionary(x => x.MountExp);
            _experiencesByAlignmentExp = DatabaseManager.Connection.Query<Experience>("SELECT * FROM experiences WHERE Level <= '10'").ToDictionary(x => x.AlignmentExp);
            _experiencesByJobExp = DatabaseManager.Connection.Query<Experience>("SELECT * FROM experiences WHERE Level <= '100'").ToDictionary(x => x.JobExp);
        }

        public long GetExperienceLevelFloor(short level)
        {
            return _experiencesByLevel[level].CharacterExp;
        }

        public long GetNextExperienceLevelFloor(short level)
        {
            if (level >= 200)
                return 7407232000;
            return _experiencesByLevel[level + 1].CharacterExp;
        }

        public short GetNextLevelExperienceFloor(long experience)
        {
            return (short)_experiencesByCharacterExp.LastOrDefault(x => x.Key <= experience).Value.Level;
        }

        public sbyte GetNextGradeHonorFloor(ushort honor)
        {
            return (sbyte)_experiencesByAlignmentExp.LastOrDefault(x => x.Key <= honor).Value.Level;
        }

        public ushort GetHonorGradeFloor(int grade)
        {
            return (ushort)_experiencesByLevel[grade].AlignmentExp;
        }

        public ushort GetHonorNextGradeFloor(int grade)
        {
            if (grade >= 10)
                return 17500;
            return (ushort)_experiencesByLevel[grade + 1].AlignmentExp;
        }

        public long GetJobExperienceLevelFloor(sbyte level)
        {
            return _experiencesByLevel[level].JobExp;
        }

        public long GetJobNextExperienceLevelFloor(sbyte level)
        {
            if (level >= 100)
                return 581687;
            return _experiencesByLevel[level + 1].JobExp;
        }

        public sbyte GetJobLevelExperienceFloor(long experience)
        {
            return (sbyte)_experiencesByJobExp.LastOrDefault(x => x.Key <= experience).Value.Level;
        }

        public sbyte GetNextJobLevelExperienceFloor(long experience)
        {
            return (sbyte)_experiencesByJobExp.LastOrDefault(x => x.Key <= experience).Value.Level;
        }
    }
}
