﻿using Dapper;
using Sunshine.Mysql.Database;
using Sunshine.MySql.Database.World.Dungeons;
using Sunshine.Protocol.Utils;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Sunshine.MySql.Database.Managers
{
    public class DungeonManager : Singleton<DungeonManager>    
    {
        public Dictionary<int, DungeonSpawn> GetAllDungeons()
        {
            return DatabaseManager.Connection.Query<DungeonSpawn>($"SELECT * FROM dungeons").ToDictionary(x => x.Map);
        }
    }
}
