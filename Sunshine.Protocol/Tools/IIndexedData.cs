using System;
namespace Sunshine.Protocol.IO.Tools
{
	public interface IIndexedData
	{
		int Id
		{
			get;
		}
	}
}
