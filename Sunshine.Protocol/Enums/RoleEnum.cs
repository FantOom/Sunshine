﻿using System;

namespace Sunshine.Protocol.Enums
{
  public enum RoleEnum
  {
    Player = 1,
    Moderator,
    GameMaster_Padawan,
    GameMaster,
    Administrator
  }
}
