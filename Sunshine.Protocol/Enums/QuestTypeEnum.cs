﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Sunshine.Protocol.Enums
{
    public enum QuestTypeEnum
    {
        QUEST_TYPE_GO_TO = 1,
        QUEST_TYPE_SHOW_TO,
        QUEST_TYPE_GET_TO,
        QUEST_TYPE_DISCOVER_MAP,
        QUEST_TYPE_DISCOVER_AREA,
        QUEST_TYPE_WIN_VS_MONSTER_ONE_FIGHT,
        QUEST_TYPE_WIN_VS_MONSTER,
        QUEST_TYPE_GO_BACK_TO,
        QUEST_TYPE_ESCORT,
        QUEST_TYPE_WIN_VS_PLAYER_ONE_DUEL,
        QUEST_TYPE_GET_SOUL_TO,
        QUEST_TYPE_ELIMINATE
    }
}
