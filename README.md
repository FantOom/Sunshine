<p align="center"><img src="/Sunshine.png" width="100%"><br></p>
<center>Emulator in C# for Dofus 2.3.7</center>

# Download
* [Client](https://mega.nz/#!XR5mETwY!8bqOWLYSazZsb4dscYetPzJ-sP8bEwPuMyqm-0rUo9k)
* [Trello](https://trello.com/b/j0VZZGJi/sunshine)

# Screens
![alt tag](https://puu.sh/yhFJg/eaa05f8a42.png)
![alt tag](https://puu.sh/xY7os/8d4adf8e02.png)
![alt tag](https://puu.sh/xY8vh/cc3bc2ba90.png)
![alt tag](https://puu.sh/xY81j/6860f9a81f.png)
![alt tag](https://puu.sh/ybvTb/c884947ca1.png)
![alt tag](https://puu.sh/yh1oH/275c778c20.png)
![alt tag](https://puu.sh/ykHOy/cb0d2df29f.png)
![alt tag](https://puu.sh/ysODF/59a9dbf4b4.png)
![alt tag](https://puu.sh/ysOhM/c17401d263.png)
![alt tag](https://puu.sh/ywPue/cdeb96bf06.png)
![alt tag](https://puu.sh/yztwM/d38ff7ff4b.png)

# Credits
* Neross (https://github.com/nerosscadernis)
* Tifoux
* Skeezr (https://github.com/skeezr)
* Taykyue (https://gitlab.com/taykyue)
* Naywyn
