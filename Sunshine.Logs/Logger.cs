﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Drawing;

namespace Sunshine.Logs
{
    public class Logger
    {
        public static void Write(string text)
        {
            Console.ForegroundColor = ConsoleColor.White;
            Console.WriteLine("{0}", text);                       
        }

        public static void WriteInfo(string info)
        {
            Console.ForegroundColor = ConsoleColor.Green;
            string _messInfo = string.Format("[ Info ] {0}", info);
            Console.WriteLine(_messInfo);
            SaveLog(_messInfo);
            Console.ForegroundColor = ConsoleColor.White;
        }

        public static void WriteError(string error)
        {
            Console.ForegroundColor = ConsoleColor.Red;
            string _messError = string.Format("[ Error ] {0}", error);
            Console.WriteLine(_messError);
            SaveLog(_messError, true);
            Console.ForegroundColor = ConsoleColor.White;
        }

        public static void WriteClient(string clt, string ip, int port, string message)
        {
            Console.ForegroundColor = ConsoleColor.Cyan;
            string _messReceive = string.Format("[ {0} ] <{2}:{3}> Receive {1}", clt, message, ip, port);
            Console.WriteLine(_messReceive);
            SaveLog(_messReceive);
            Console.ForegroundColor = ConsoleColor.White;
        }

        public static void WriteServer(string srv, string ip, int port, string message)
        {
            Console.ForegroundColor = ConsoleColor.Cyan;
            string _messSend = string.Format("[ {0} ] <{2}:{3}> Send {1}", srv, message, ip, port);
            Console.WriteLine(_messSend);
            SaveLog(_messSend);
            Console.ForegroundColor = ConsoleColor.White;
        }

        private static void SaveLog(string message, bool error = false)
        {
            try
            {
                string m_path = "";
                string m_date = System.DateTime.Now.ToShortDateString();
                string m_timenow = string.Format("{0} {1} ", m_date, System.DateTime.Now.ToLongTimeString());

                if (error)
                    m_path = string.Format(".\\logs\\errorlog_{0}.txt", m_date.Replace('/', '-'));
                else
                    m_path = string.Format(".\\logs\\log_{0}.txt", m_date.Replace('/', '-'));

                using (System.IO.StreamWriter logs = new System.IO.StreamWriter(m_path, true))
                {
                    logs.WriteLine(message);
                }
            }
            catch (Exception)
            {
                return;
            }
            
        }
    }
}
